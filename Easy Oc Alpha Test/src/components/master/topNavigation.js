import React, { Component } from 'react';
import { MDBFormInline, MDBNavbar, MDBBtn, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon } from 'mdbreact';
import Firebase from '../../Firebase';

class TopNavigation extends Component {
    constructor(props){
        super(props);

        this.logout = this.logout.bind(this);
        this.state = {
            collapse: false
        }

    }
    
    /*Función para desloguearse de firebase */
    logout(){
        Firebase.auth().signOut();
    }

    onClick = () => {
        this.setState({
            collapse: !this.state.collapse,
        });
    }
    /*Función para abrir un Modal */
    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {
        return (
            <MDBNavbar className="flexible-navbar" light expand="md" scrolling>
                <MDBNavbarBrand className="justify-content-center" href="/">
                    <strong>EasyOC</strong>
                </MDBNavbarBrand>

                <MDBNavbarNav left>
                    </MDBNavbarNav>
                {/*If para preguntar si el cliente está logueado, muestra botón de deslogueo, sino no */}
                {this.props.hiddenbtn ? (<button style={{color:'gray'}} className="py-2 btn btn-flat btn-lg waves-effect waves-effect" href="/" onClick={this.logout}  size="sm" color="red lighten-2" >
                <MDBIcon style={{color:'gray'}} className='pr-1'  icon="user"/>{this.props.hiddenbtn.displayName}<MDBIcon style={{color: '#f44336'}} className="pl-3" icon="sign-out-alt" size="lg" />
                </button>) : null}{/*Muestra el Nombre asociado a la cuenta de firebase que está logueado*/}
            
            </MDBNavbar>
        );
    }
}

export default TopNavigation;