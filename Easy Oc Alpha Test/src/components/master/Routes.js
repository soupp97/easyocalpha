/*se importan todos los componentes a utilizar */
import React from 'react';
import firebase from '../../Firebase';
import { Route, Switch } from 'react-router-dom';
import DashboardPage from '../pages/DashboardPage';
import Login from '../pages/Login';
import NuevaSolicitud from '../pages/sections/solicitante/NuevaSolPage';
import ReportesSection from '../pages/sections/admin/ReportesSection';
import SuperAdminCrud from '../pages/sections/superadmin/SuperAdminCrud';
import VerSolicitudPage from '../pages/sections/solicitante/VerSolicitudPage';
import ProveedorCrud from '../pages/sections/proveedor/ProveedorCrud';
import CECOCrud from '../pages/sections/admin/AdminCrudCECO';
import SACrudProveedor from '../pages/sections/admin/SACrudProveedor';
import ProveedorSolicitud from '../pages/sections/proveedor/ProveedorSolicitud';
import AreaCrud from '../pages/sections/admin/CrudArea';


/*Clase Ruoutes que contiente la arquitectura del paginado */
class Routes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rutas: '',
      usuario: {},
      proveedor:{},
      key: '',
      key2: '',
      currentUsr: '',
      routAdm: ''
    }
  }

  componentDidMount() {
    const CurrentUs = firebase.auth().currentUser.uid;
    const ref = firebase.firestore().collection('usuario').doc(CurrentUs);
    ref.get().then((doc) => {
      if (doc.exists) {
        this.setState({
          usuario: doc.data(),
          key: doc.id,
          isLoading: false
        });
        //IF para preguntar si el cargo es Administrador o Solicitante muestra Dashboard, si es Proveedor solo muestra proveedor
        if (this.state.usuario.cargo == 'Administrador') {
          this.setState({
            rutas: <Switch>
              <Route path='/' exact render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
              <Route path='/dashboard' render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} /> />
              <Route path='/login' render={(routeProps) => (<Login {...routeProps} data={this.props.data} />)} />
              <Route path='/supercrud' render={(routeProps) => (<SuperAdminCrud {...routeProps} data={this.props.data} />)} />
              <Route path='/agregar_sol' render={(routeProps) => (<NuevaSolicitud {...routeProps} data={this.props.data} />)} />
              <Route path='/reportes' render={(routeProps) => (<ReportesSection {...routeProps} data={this.props.data} />)} />
              <Route path='/versolicitud/:id' render={(routeProps) => (<VerSolicitudPage {...routeProps} data={this.props.data} />)} />
              <Route path='/cecocrud' render={(routeProps) => (<CECOCrud {...routeProps} data={this.props.data} />)} />
              <Route path='/SACrudprov' render={(routeProps) => (<SACrudProveedor {...routeProps} data={this.props.data} />)} />
              <Route path='/AreaCrud' render={(routeProps) => (<AreaCrud {...routeProps} data={this.props.data} />)} />
            </Switch>
          });
        } else if (this.state.usuario.cargo == 'Solicitante'){
          this.setState({
            rutas: <Switch>
              <Route path='/' exact render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
              <Route path='/dashboard' render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
              <Route path='/login' render={(routeProps) => (<Login {...routeProps} data={this.props.data} />)} />
              <Route path='/agregar_sol' render={(routeProps) => (<NuevaSolicitud {...routeProps} data={this.props.data} />)} />
              <Route path='/reportes' render={(routeProps) => (<ReportesSection {...routeProps} data={this.props.data} />)} />
              <Route path='/versolicitud/:id' render={(routeProps) => (<VerSolicitudPage {...routeProps} data={this.props.data} />)} />
            </Switch>
          });
        } else if (this.state.usuario.cargo == 'Operador'){
          this.setState({
            rutas: <Switch>
              <Route path='/' exact render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
              <Route path='/dashboard' render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
              <Route path='/login' render={(routeProps) => (<Login {...routeProps} data={this.props.data} />)} />
              <Route path='/agregar_sol' render={(routeProps) => (<NuevaSolicitud {...routeProps} data={this.props.data} />)} />
              <Route path='/reportes' render={(routeProps) => (<ReportesSection {...routeProps} data={this.props.data} />)} />
              <Route path='/versolicitud/:id' render={(routeProps) => (<VerSolicitudPage {...routeProps} data={this.props.data} />)} />
            </Switch>
            });
        }  else if (this.state.usuario.cargo == 'Jefe Administrativo'){
          this.setState({
            rutas: <Switch>
              <Route path='/' exact render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
              <Route path='/dashboard' render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
              <Route path='/login' render={(routeProps) => (<Login {...routeProps} data={this.props.data} />)} />
              <Route path='/agregar_sol' render={(routeProps) => (<NuevaSolicitud {...routeProps} data={this.props.data} />)} />
              <Route path='/reportes' render={(routeProps) => (<ReportesSection {...routeProps} data={this.props.data} />)} />
              <Route path='/versolicitud/:id' render={(routeProps) => (<VerSolicitudPage {...routeProps} data={this.props.data} />)} />
            </Switch>
            });
        } else if (this.state.usuario.cargo == 'Super Administrador'){
          this.setState({
            rutas: <Switch>
            <Route path='/' exact render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
            <Route path='/dashboard' render={(routeProps) => (<DashboardPage {...routeProps} data={this.props.data} />)} />
            <Route path='/login' render={(routeProps) => (<Login {...routeProps} data={this.props.data} />)} />
            <Route path='/supercrud' render={(routeProps) => (<SuperAdminCrud {...routeProps} data={this.props.data} />)} />
            <Route path='/agregar_sol' render={(routeProps) => (<NuevaSolicitud {...routeProps} data={this.props.data} />)} />
            <Route path='/reportes' render={(routeProps) => (<ReportesSection {...routeProps} data={this.props.data} />)} />
            <Route path='/versolicitud/:id' render={(routeProps) => (<VerSolicitudPage {...routeProps} data={this.props.data} />)} />
            <Route path='/cecocrud' render={(routeProps) => (<CECOCrud {...routeProps} data={this.props.data} />)} />
            <Route path='/SACrudprov' render={(routeProps) => (<SACrudProveedor {...routeProps} data={this.props.data} />)} />
            <Route path='/AreaCrud' render={(routeProps) => (<AreaCrud {...routeProps} data={this.props.data} />)} />
          </Switch>
            });
        }
      } else {
        this.setState({
          rutas: <Switch>
            <Route path='/' render={(routeProps) => (<ProveedorCrud {...routeProps} data={this.props.data} />)} />
            <Route path='/proveedorsoli' render={(routeProps) => (<ProveedorSolicitud {...routeProps} data={this.props.data} />)} />
          </Switch>
        });
      }
    });
    


    }
  
  

  render() {
    return (
       //llamamos a las rutas según usuario
      this.state.rutas
      

    );
  }
}

export default Routes;
