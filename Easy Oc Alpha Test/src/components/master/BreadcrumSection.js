import React from 'react';
import { MDBCard, MDBCardBody, MDBIcon, MDBBreadcrumb, MDBBreadcrumbItem, MDBFormInline, MDBBtn } from 'mdbreact';
//Sección Breadcrumb que no está funcional, es sólo maqueta.
const BreadcrumSection = () => {
  return (
    <MDBCard className="mb-4">
      <MDBCardBody style={{margin:"-10px",padding:"-10px"}} id="breadcrumb" className="d-flex  justify-content-between">
        <MDBBreadcrumb className="md-form m-0 py-1" style={{margin:"-20px",padding:"-20px"}}>
          <MDBBreadcrumbItem >Home</MDBBreadcrumbItem>
          <MDBBreadcrumbItem active>Dashboard</MDBBreadcrumbItem>
        </MDBBreadcrumb>
      </MDBCardBody>
    </MDBCard>
  )
}

export default BreadcrumSection;

