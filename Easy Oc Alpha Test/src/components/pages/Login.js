import React, { Component } from "react";
import { MDBCard, MDBCol, MDBInput, MDBRow, MDBCardImage, MDBCardBody, MDBCardTitle, MDBIcon } from 'mdbreact';
import src1 from '../../assets/img-1.jpg';
import Firebase from "../../Firebase";
import { Route, Switch } from 'react-router-dom';

class Login extends Component {
    constructor(props) {
        super(props);
        this.refProv = Firebase.firestore().collection('proveedor');
        this.login = this.login.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            usuario: [],
            proveedor: [],
            correo_contacto:'',
            usuarioLogin: {},
            cargoLogin: '',
            userType: '',
            email: '',
            contra: '',
            password: '',
            estado: '',
            cargo: '',
            mail: ''
        }
    }
    login(e) {
        e.preventDefault();

    }
    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value
        this.setState(state);
        console.log(this.state.cargoLogin);
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        for (let index = 0; index < this.props.data.usuario.length; index++) {
            const usu = this.props.data.usuario[index];
            if (usu.data.mail == this.state.email) {
                if (usu.data.cargo == this.state.cargoLogin) {
                    Firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) => { }).catch((error) => {
                        var code = error.code;
                        var message = error.message;
                        if (code == null) {
                            this.setState({ estado: '' });
                        } else {
                            this.setState({ estado: "Los datos son incorrectos" });
                        }
                    });
                } else {
                    if (this.state.cargoLogin == 'Seleccione Usuario' || this.state.cargoLogin == null) {
                        this.setState({ estado: "Ingrese Cargo " });
                    } else {
                        this.setState({ estado: "El usuario no posee privilegios de " + this.state.cargoLogin });
                    }
                }
            }
        }
        for (let index = 0; index < this.state.proveedor.length; index++) {
            const pro = this.state.proveedor[index];
            if (pro.correo_contacto == this.state.email) {
                if("Proveedor" == this.state.cargoLogin){
                    Firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) => { }).catch((error) => {
                        var code = error.code;
                        var message = error.message;
                        if (code == null) {
                            this.setState({ estado: '' });
                        } else {
                            this.setState({ estado: "Los datos son incorrectos" });
                        }
                    });

                }else {
                    if(this.state.cargoLogin == 'Seleccione Usuario' || this.state.cargoLogin == null){
                        this.setState({ estado: "Ingrese Cargo " });
                    }else{
                        this.setState({ estado: "El usuario no posee privilegios de "+ this.state.cargoLogin });
                    }   
                }
            }
        }

    }

    

    onCollectionUpdateProv = (querySnapshot) => {
        const proveedor = [];
        querySnapshot.forEach((doc) => {
            const { correo_contacto } = doc.data();
            proveedor.push({
                key: doc.id,
                doc, // DocumentSnapshot,
                correo_contacto
            });
        });
        this.setState({
            proveedor
        });
    }

    componentDidMount() {
        this.unsubscribe = this.refProv.onSnapshot(this.onCollectionUpdateProv);
        console.log(this.props.data);
    }
    render() {
        return (

            <div>
                
                <MDBRow className="justify-content-center">
                    <MDBCol sm="12" md="6" lg="3" className="mb-5">
                        <MDBCard>
                            <MDBCardImage className="img-fluid" src={src1} />
                            <MDBCardBody>
                                <MDBCardTitle className="text-center mb-2 font-bold">Log In</MDBCardTitle>
                                <form onSubmit={this.handleSubmit}>
                                    <div className="text-center">
                                        <div className="input-group mt-3 mb-3">
                                            <div className="input-group-prepend">
                                                <label className="input-group-text" for="inputGroupSelect01"><MDBIcon icon="user-circle" /></label>
                                            </div>
                                            <select onChange={this.onChange} className="custom-select" name="cargoLogin">
                                                <option value="Seleccione Usuario" selected>Seleccione Usuario</option>
                                                <option value="Solicitante">Solicitante</option>
                                                <option value="Jefe Administrativo">Jefe Administrativo</option>
                                                <option value="Operador">Operador</option>
                                                <option value="Administrador">Administrador</option>
                                                <option value="Super Administrador">Super Administrador</option>
                                                <option value="Proveedor">Proveedor</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="input-field">
                                        <MDBInput value={this.state.email} type="email" id="email" label="Correo" outline icon="envelope" onChange={this.handleChange} />
                                    </div>
                                    <div className="input-field">
                                        <MDBInput value={this.state.password} type="password" id="password" label="Contraseña" outline icon="key" onChange={this.handleChange} />
                                    </div>
                                    {this.state.estado == '' ? null : (<h6 style={{ color: "red" }}>{this.state.estado}</h6>)}
                                    <div className="input-field text-center">
                                        <button type="submit" href="/dashboard" className="btn btn-primary Ripple-parent">Entrar <MDBIcon icon="sign-in-alt"></MDBIcon></button>
                                    </div>

                                </form>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </div>
        )
    }
}

export default Login