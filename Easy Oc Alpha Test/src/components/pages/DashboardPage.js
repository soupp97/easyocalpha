//Importaciones de los módulos para usar en esta sección
import React, { Component } from 'react';
import firebase from '../../Firebase'
import { MDBBtn, MDBModal, MDBModalBody, MDBCardHeader, MDBModalHeader, MDBTable, MDBTableHead, MDBTableBody, MDBCardBody, MDBCard, MDBIcon, MDBRow, MDBContainer, MDBCol, MDBCardText } from 'mdbreact';
import ListaSolicitudDashboard from './sections/admin/ListaSolicitudDashboard';
import DashboardEstadoCards from './sections/admin/DashboardEstadoCards';
import NumberFormat from 'react-number-format'; //Libreria para formatear presupuesto
//Clase que contendrá la base del DashBoard
class DashboardPage extends Component {
  constructor(props) {
    super(props);
    this.refCECO = firebase.firestore().collection('centro_costo');
    this.state = {
      usuario: [],
      CECO: [],
      us :{},
      key:'',
      btnAdmin : null,
      CECOus:'',
      nomrbe:'',//Nombre del ceco
      nro_ceco:'',
      usKey:'',
      presupuesto:'',
      key: ''
    }
  }

  state = {//Declaramos variables locales
    
    modal1: false,
    modal2: false,
    centroCO: '',
    presupUs:'',
    modal3: false
    
  }

  onCollectionUpdateCECO = (querySnapshot) => {
    const CECO = [];
    const nombreCECO = '';
    querySnapshot.forEach((doc) => {
        const { nombre, nro_ceco, presupuesto } = doc.data();
        CECO.push({
            key: doc.id,
            doc, // DocumentSnapshot
            nombre,
            nro_ceco,
            presupuesto
        });
    });
    this.setState({
        CECO
    });

  }

  obtenerCECOus(){
    this.props.data.CECO.map(ceco => ceco.key == this.props.data.currentUs.CECOus ? 
      this.setState({ presupUs : <h4><NumberFormat value={ceco.presupuesto} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h4>}):null)
  }

    

componentDidMount() {
  console.log(this.props.data);
  if(this.props.data.currentUs.cargo == 'Administrador' || this.props.data.currentUs.cargo == 'Super Administrador'){
    this.setState({btnAdmin : <MDBBtn size="lg" block color="primary" onClick={this.toggle(9)} >Administración</MDBBtn> });
  }else {
    this.setState({btnAdmin : null});
  }
  this.setState({centroCO : this.props.data.currentUs.CECOus});
  this.unsubscribe = this.refCECO.onSnapshot(this.onCollectionUpdateCECO);
  this.obtenerCECOus();
  

  }
  





  toggle = nr => () => { //función para que aparezca y desaparezca el modal
    let modalName = 'modal' + nr;
    this.setState({
      [modalName]: !this.state[modalName]
    })
  }
  //Contenido que retornará este módulo
  render() {
    const {cargoLogin} = this.state;
    return (
      <MDBContainer fluid>
        
        <MDBRow>
          <MDBCol>
            <MDBRow className="div2">
              <MDBCol className="texet-center">
                <MDBCard className="text-center">
                  <MDBCardHeader className="p-1">Centro de Costo</MDBCardHeader>
                  <MDBCardBody className="p-1">
                  {this.state.CECO.map(ceco => ceco.key == this.state.centroCO ? 
                  <h4>{ceco.nombre+' '+ceco.nro_ceco}</h4> : null)}
                  </MDBCardBody>
                  <MDBCardHeader className="p-1" >Presupuesto General</MDBCardHeader>
                  <MDBCardBody className="p-1">
                  {this.state.CECO.map(ceco => ceco.key == this.state.centroCO ? 
                  <h4><NumberFormat value={ceco.presupuesto} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h4> : null)}
                  {this.state.presupUs}
                  </MDBCardBody>

                </MDBCard>
              </MDBCol>
            </MDBRow>
            {/* --Sección de Estadísticas-- */}
            <DashboardEstadoCards Us={this.props.data.currentUs} KeyUs={this.props.data.keyA}/>
            <MDBRow>{/* --Boton Agregar Solicitud-- */}
              <MDBCol>
                <MDBBtn size="lg" block color="primary" href="/agregar_sol"><MDBIcon icon="plus-circle" /> Solicitud</MDBBtn>
                {/* --Modal Agregar Solicitud-- */}
              </MDBCol>
              <MDBCol className="div2">
                {/* --Boton Reportes-- */}
                <MDBBtn size="lg" block color="primary" href="/reportes"> Reportes</MDBBtn>
              </MDBCol>
              <MDBCol className="div2">
                {/* --Botón Administración-- */}
                {this.state.btnAdmin}
                {/* --Modal Administración-- */}
                <MDBModal toggle={this.toggle(9)} isOpen={this.state.modal9} backdrop={true}>
                  <MDBModalHeader className="text-center">¿Que desea administrar?</MDBModalHeader>
                  <MDBModalBody className="text-center">
                    <MDBRow>
                      <MDBCol className="my-2 col-6" >
                        <MDBBtn block color="green" size="lg" md="4" href="/supercrud" >Usuarios</MDBBtn>
                        <MDBBtn block color="green" size="lg" md="4" href="/SACrudprov" >Proveedores</MDBBtn>
                        <MDBBtn block color="green" size="lg" md="4" href="/AreaCrud" >Areas</MDBBtn>
                        </MDBCol>
                      <MDBCol className="my-2 col-6">
                      <MDBBtn block color="green" size="lg" md="4" href="/#" >Bitácoras</MDBBtn>
                      <MDBBtn block color="green" size="lg" md="4" href="/#" >Cotizaciones</MDBBtn>
                      <MDBBtn block color="green" size="lg" md="4" href="/cecocrud" >Presupuestos</MDBBtn>
                      </MDBCol>
                    </MDBRow>
                    <MDBRow>
                      <MDBCol ><MDBBtn color="primary" size="sm" md="4" onClick={this.toggle(9)}>Volver</MDBBtn></MDBCol>
                    </MDBRow>
                  </MDBModalBody>
                </MDBModal>
              </MDBCol>
            </MDBRow>
          </MDBCol>
          <MDBCol>
            <ListaSolicitudDashboard KeyUs={this.props.data.keyA} Us={this.props.data.currentUs} />
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    )
  }
}

export default DashboardPage;

