import React, { Component } from 'react';
import { MDBDataTable, MDBCard, MDBCardHeader, MDBCardBody, MDBIcon, MDBBtn, MDBModal, MDBCol, MDBContainer, MDBInput, MDBModalBody, MDBRow, MDBModalHeader } from "mdbreact";
import { Table, Container } from 'reactstrap';
import firebase from '../../../../Firebase';
import FirebaseUsers from '../../../../FirebaseUsers';
import { userInfo } from 'os';
import { validate, clean, format } from 'rut.js';



class SuperAdmin extends Component {
    constructor(props) {
        super(props);
        this.ref = firebase.firestore().collection('usuario');
        this.refCECO = firebase.firestore().collection('centro_costo');
        this.unsubscribe = null;
        this.state = {
            usuario: [],
            CECO: [],
            CECO_UID: '',
            ceco_key: '',//Variable para CECO Key
            num_cuenta: '',
            CECOus: '',
            rut: '',
            nombres: '',
            apellidos: '',
            mail: '',
            uid: '',
            cargo: '',
            fono: '',
            contraseña: '',
            contraseñaConf: '',
            key: ''
        };
    }

    state = {
        rutval: '',
        pass: '',
        passConf: '',
        usr: '',
        modalUsIngresado: false,
        modal1: false,//Modal Agregar
        modal2: false,
        modal3: false,
        modal4: false,
        modal5: false,
        modal6: false
    }

    limpiarEdt = (tg) => () => {
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            rut: '',
            nombres: '',
            apellidos: '',
            mail: '',
            cargo: '',
            fono: '',
            contraseña: '',
            contraseñaConf: '',
            key: ''
        })
    }

    confirmarEditar = (tg1, tg2, tg3) => () => {
        let modalName1 = 'modal' + tg1;
        let modalName2 = 'modal' + tg2;
        let modalName3 = 'modal' + tg3;
        this.setState({
            [modalName1]: !this.state[modalName1],
            [modalName2]: !this.state[modalName2],
            [modalName3]: !this.state[modalName3]
        })
    }

    eliminar = (usr1, tg) => () => {
        console.log(tg, usr1);
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            usr: usr1
        })


    }
    editar = (usr1, tg) => () => {
        const ref2 = FirebaseUsers.firestore().collection('usuario').doc(usr1);
        ref2.get().then((doc) => {
            if (doc.exists) {
                const user1 = doc.data();
                this.setState({
                    key: doc.id,
                    rut: user1.rut,
                    nombres: user1.nombres,
                    apellidos: user1.apellidos,
                    mail: user1.mail,
                    uid: user1.uid,
                    cargo: user1.cargo,
                    contraseña: user1.contraseña,
                    contraseñaConf: user1.contraseña,
                    CECOus: user1.CECOus,
                    fono: user1.fono
                });
            } else {
                console.log("No such document!");
            }
            FirebaseUsers.auth().signOut();
        });
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
        })


    }

    toggle = nr => () => {
        let modalName = 'modal' + nr;
        this.setState({
            [modalName]: !this.state[modalName]
        })
    }


    onChange = (e) => {
        const state = this.state
        if (e.target.name == 'rut') {
            e.target.value = clean(e.target.value);
            e.target.value = format(e.target.value);
            state[e.target.name] = e.target.value
            this.setState(state);
            validate(e.target.value) ?
                this.setState({ rutval: <p style={{ color: 'green' }}>Rut valido <MDBIcon icon="check-circle"></MDBIcon></p> }) :
                this.setState({ rutval: <p style={{ color: 'red' }}>Rut no valido <MDBIcon icon="times-circle"></MDBIcon></p> });
            console.log(e.target.value);
        } else {
            state[e.target.name] = e.target.value
            this.setState(state);
            console.log(e.target.name);
        }

    }


    //Función para Editar en Firebase
    onSubmit2 = (e) => {
        e.preventDefault();
        //className para  validar campos
        e.target.className += "was-validated";
        const { rut, nombres, apellidos, mail, cargo, CECOus, contraseña, fono } = this.state;
        if (!validate(rut)) {
        } else if (nombres.trim().length <= 0) {
        } else if (apellidos.trim().length <= 0) {
        } else if (mail.trim().length <= 0) {
        } else if (cargo.trim().length <= 0) {
        } else if (CECOus.trim().length <= 0) {
        } else if (fono.trim().length <= 0) {
        } else {
            //referencia para editar 
            const updateRef = FirebaseUsers.firestore().collection('usuario').doc(this.state.key);
            updateRef.set({
                rut,
                nombres,
                apellidos,
                mail,
                cargo,
                CECOus,
                fono
            }).then((docRef) => {
                this.setState({
                    key: '',
                    rut: '',
                    nombres: '',
                    apellidos: '',
                    mail: '',
                    cargo: '',
                    CECOus: '',
                    contraseña: '',
                    contraseñaConf: '',
                    fono: ''
                });
                this.props.history.push("#");

            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
            FirebaseUsers.auth().signOut();
        }
    }

    //Función para Agregar en Firebase
    onSubmit = (e) => {
        e.preventDefault();
        //Agregamos classname para validar campos
        e.target.className += "was-validated";
        const { rut, nombres, apellidos, mail, contraseña, contraseñaConf, cargo, CECOus, fono } = this.state;
        if (!validate(rut)) {
        } else if (nombres.trim().length <= 0) {
        } else if (apellidos.trim().length <= 0) {
        } else if (mail.trim().length <= 0) {
        } else if (cargo.trim().length <= 0) {
        } else if (fono.trim().length <= 0) {
        } else if (contraseña.length <= 5) {
        } else if (CECOus.trim().length <= 0) {
        } else if (contraseñaConf != contraseña) {
        } else {
            FirebaseUsers.auth().createUserWithEmailAndPassword(this.state.mail, this.state.contraseña).then((cred) => {
                cred.user.updateProfile({
                    displayName: nombres + ' ' + apellidos
                }).then();
                this.ref.doc(cred.user.uid).set({
                    rut,
                    nombres,
                    apellidos,
                    mail,
                    cargo,
                    CECOus,
                    contraseña,
                    fono
                }).then((docRef) => {
                    this.setState({
                        rut: '',
                        nombres: '',
                        apellidos: '',
                        mail: '',
                        cargo: '',
                        CECOus: '',
                        contraseña: '',
                        contraseñaConf: '',
                        fono: ''
                    });
                    let modalName2 = 'modal1';
                    this.setState({
                        [modalName2]: !this.state[modalName2]
                    });
                    let modalName = 'modalUsIngresado';
                    this.setState({
                        [modalName]: !this.state[modalName]
                    });
                    this.props.history.push("#")

                })
                    .catch((error) => {
                        console.error("Error adding document: ", error);
                    });

            }).catch((error) => {
                var code = error.code;
                var message = error.message;
                if (code == null) {
                    this.setState({ estado: "" });
                } else {
                    this.setState({ estado: "El Email o la contraseña son incorrectos." });
                }
                console.log(code);
            });
            FirebaseUsers.auth().signOut();
        }
    }
    onCollectionUpdate = (querySnapshot) => {
        const usuario = [];
        querySnapshot.forEach((doc) => {
            const { rut, nombres, apellidos, CECOus, contraseña, uid, mail, cargo, fono } = doc.data();
            usuario.push({
                key: doc.id,
                doc, // DocumentSnapshot
                rut,
                nombres,
                apellidos,
                mail,
                CECOus,
                cargo,
                contraseña,
                uid,
                fono
            });
        });
        this.setState({
            usuario
        });
    }


    onCollectionUpdateCECO = (querySnapshot) => {
        const CECO = [];
        querySnapshot.forEach((doc) => {
            const { nombre, nro_ceco, presupuesto } = doc.data();
            CECO.push({
                key: doc.id,
                doc, // DocumentSnapshot
                nombre,
                nro_ceco,
                presupuesto
            });
        });
        this.setState({
            CECO
        });

    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
        this.unsubscribe = this.refCECO.onSnapshot(this.onCollectionUpdateCECO);
    }
    //Eliminar usuario
    delete(id, tg, tg2) {
        FirebaseUsers.firestore().collection('usuario').doc(id).delete().then(() => {

            this.props.history.push("#")

        }).catch((error) => {
            console.error("Error removing document: ", error);
        });
        let modalName = 'modal' + tg;
        let modalName2 = 'modal' + tg2;
        this.setState({
            [modalName]: !this.state[modalName],
            [modalName2]: !this.state[modalName2]
        })
        FirebaseUsers.auth().signOut();
    }
    render() {
        const { rut, nombres, apellidos, mail, cargo, contraseña, contraseñaConf, CECOus, fono, usr } = this.state;
        return (
            <Container>
                <MDBCard>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <MDBCardHeader><h3>Gestión de Usuarios</h3></MDBCardHeader>
                        </div>
                        {/*tabla de administradores */}
                        <Table bordered responsive striped>
                            <thead>
                                <tr>
                                    <th>Rut</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Mail</th>
                                    <th>Cargo</th>
                                    <th>Fono</th>
                                    <th>CECO</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.usuario.map(usuario =>
                                    <tr>
                                        <td>{usuario.rut}</td>
                                        <td>{usuario.nombres}</td>
                                        <td>{usuario.apellidos}</td>
                                        <td>{usuario.mail}</td>
                                        <td>{usuario.cargo}</td>
                                        <td>{usuario.fono}</td>
                                        {this.state.CECO.map(ceco => ceco.key == usuario.CECOus ?
                                            <td>{ceco.nombre + ' ' + ceco.nro_ceco}</td> : null)}
                                        <td scope="row">
                                            <MDBBtn color="yellow" onClick={this.editar(usuario.key, 2)} ><MDBIcon size="lg" icon="edit" /></MDBBtn>
                                            <MDBBtn color="red" onClick={this.eliminar(usuario.key, 5)} ><MDBIcon size="lg" icon="times-circle" /></MDBBtn>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        {/*-Sección Modal Eliminar */}
                        <MDBModal size="md" toggle={this.toggle(5)} isOpen={this.state.modal5} >
                            <MDBModalHeader>¿Desea eliminar esto?</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer fluid>
                                    <MDBRow middle="true" className="mb-5">
                                        <MDBCol>
                                            <h5 style={{ fontSize: '17px' }}>Eliminar un Usuario Impone:</h5>
                                            <h5 style={{ color: 'red', fontSize: '17px' }}>Eliminar <strong>TODA</strong> la información de este perfil</h5>
                                            <h5 style={{ fontSize: '17px' }}>Sabiendo esto, ¿Continuar con la eliminación?</h5>
                                        </MDBCol>
                                    </MDBRow>
                                    <MDBRow middle="true">
                                        <MDBCol className="mx-2">
                                            <MDBBtn block color="red" size="md" onClick={this.delete.bind(this, usr, 5, 6)} >Si</MDBBtn>
                                        </MDBCol>
                                        <MDBCol className="mx-2">
                                            <MDBBtn block color="blue" size="md" onClick={this.toggle(5)}>No</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>

                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>
                        {/*Botón para Agregar */}
                        <MDBBtn size="md" md="4" block color="green" onClick={this.toggle(1)}>Agregar usuario</MDBBtn>
                        {/*-Sección Modal Agregar */}
                        <MDBModal isOpen={this.state.modal1} size="md" >
                            <MDBModalHeader>Ingrese los datos del Usuario</MDBModalHeader>
                            <MDBModalBody >
                                <form noValidate onSubmit={this.onSubmit}>
                                    <MDBRow>
                                        <MDBCol>

                                            <MDBInput label="RUT" outline required className="form-control" name="rut" value={rut} onChange={this.onChange}  ></MDBInput>

                                            <MDBInput label="Nombres" outline required className="form-control" name="nombres" onChange={this.onChange} value={nombres}  ></MDBInput>

                                            <MDBInput label="Apellidos" outline required className="form-control" name="apellidos" onChange={this.onChange} value={apellidos} ></MDBInput>

                                            <MDBInput label="Contraseña" outline required type="password" className="form-control" name="contraseña" onChange={this.onChange} value={contraseña} ></MDBInput>
                                            {/*Sección agregar CECO*/}
                                            <select required name="CECOus" onChange={this.onChange} className="browser-default custom-select" id="CECOus">
                                                <option value="">Seleccione CECO</option>
                                                {this.state.CECO.map(CECO =>
                                                    <option value={CECO.key}>{CECO.nombre} {CECO.nro_ceco}</option>
                                                )}
                                            </select>
                                        </MDBCol>
                                        <MDBCol>

                                            <MDBInput label="Correo" outline required className="form-control" name="mail" type="email" onChange={this.onChange} value={mail} ></MDBInput>

                                            <select required name="cargo" value={cargo} onChange={this.onChange} className="custom-select" id="slcCargo">
                                                <option value="">Seleccione Cargo</option>
                                                <option value="Solicitante">Solicitante</option>
                                                <option value="Jefe Administrativo">Jefe Administrativo</option>
                                                <option value="Operador">Operador</option>
                                                <option value="Administrador">Administrador</option>
                                                <option value="Super Administrador">Super Administrador</option>
                                            </select>


                                            <MDBInput label="Fono" outline required className="form-control" name="fono" onChange={this.onChange} value={fono}  ></MDBInput>

                                            <MDBInput label="Confirmar Contraseña" outline required type="password" className="form-control" name="contraseñaConf" onChange={this.onChange} value={contraseñaConf}  ></MDBInput>

                                        </MDBCol>
                                    </MDBRow>
                                    {/*Botones Agregar Admin */}
                                    <MDBRow>
                                        <MDBCol className="text-center">
                                            <MDBBtn type="submit" color="green" size="md" >Agregar</MDBBtn>
                                            <MDBBtn color="primary" size="md" onClick={this.toggle(1)}>Volver</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>
                                    <MDBRow>
                                        {/*Indicador de Rut válido */}
                                        <MDBCol className="text-center">
                                            {this.state.rutval}
                                        </MDBCol>
                                    </MDBRow>
                                </form>
                            </MDBModalBody>
                        </MDBModal>
                        {/*Sección Modal Editar */}
                        <MDBModal isOpen={this.state.modal2} size="md" >
                            <MDBModalHeader>Ingrese los datos del Usuario</MDBModalHeader>
                            <MDBModalBody >
                                <MDBCol >
                                    <form noValidate onSubmit={this.onSubmit2}>
                                        <MDBRow>
                                            <MDBCol>

                                                <MDBInput label="RUT" outline required className="form-control" name="rut" value={rut} onChange={this.onChange}  ></MDBInput>

                                                <MDBInput label="Nombres" outline required className="form-control" name="nombres" onChange={this.onChange} value={nombres}  ></MDBInput>

                                                <MDBInput label="Apellidos" outline required className="form-control" name="apellidos" onChange={this.onChange} value={apellidos} ></MDBInput>
                                                {/*Firebase aún no permite cambio a cualquier usuario, por lo que tanto la contraseña como el email no se pueden cambiar una vez ingresados 
                                                    <MDBInput label="Contraseña" outline required type="password" className="form-control" name="contraseña" onChange={this.onChange} value={contraseña} ></MDBInput>
                                                */}
                                                <select required value={CECOus} name="CECOus" onChange={this.onChange} className="browser-default form-control md-2 custom-select" id="CECOus">
                                                    <option value="">Seleccione CECO</option>
                                                    {this.state.CECO.map(CECO =>
                                                        <option value={CECO.key}>{CECO.nombre} {CECO.nro_ceco}</option>
                                                    )}
                                                </select>

                                                {/* Con lo dicho más arriba, no se puede modificar el correo
                                                <MDBInput label="Correo" outline required className="form-control" name="mail" type="email" onChange={this.onChange} value={mail} ></MDBInput>
                                                */}
                                                <select required name="cargo" value={cargo} onChange={this.onChange} className="custom-select mt-4 form-control" id="slcCargo">
                                                    <option value="">Seleccione Cargo</option>
                                                    <option value="Solicitante">Solicitante</option>
                                                    <option value="Jefe Administrativo">Jefe Administrativo</option>
                                                    <option value="Operador">Operador</option>
                                                    <option value="Administrador">Administrador</option>
                                                    <option value="Super Administrador">Super Administrador</option>
                                                </select>


                                                <MDBInput label="Fono" outline required className="form-control" name="fono" onChange={this.onChange} value={fono}  ></MDBInput>
                                                {/*
                                                <MDBInput label="Confirmar Contraseña" outline required type="password" className="form-control" name="contraseñaConf" onChange={this.onChange} value={contraseñaConf}  ></MDBInput>
                                                */}

                                            </MDBCol>
                                        </MDBRow>
                                        {/*Botones Editar Admin */}
                                        <MDBRow>
                                            <MDBCol className="text-center">
                                                <MDBBtn color="warning" size="md" onClick={this.toggle(3)}>Editar</MDBBtn>
                                                <MDBBtn color="primary" size="md" onClick={this.limpiarEdt(2)}>Volver</MDBBtn>
                                            </MDBCol>
                                        </MDBRow>
                                        <MDBRow>
                                            {/*Indicador de rut válido */}
                                            <MDBCol className="text-center">
                                                {this.state.rutval}
                                            </MDBCol>
                                        </MDBRow>
                                        {/*Sección Modal Confirmar Editar */}
                                        <MDBModal className="pl-5" size="sm" toggle={this.toggle(3)} isOpen={this.state.modal3} backdrop={true}>
                                            <MDBModalHeader>¿Está seguro?</MDBModalHeader>
                                            <MDBModalBody>
                                                <MDBContainer className="text-center">
                                                    Está seguro que desea editar este Administrador?

                                                    <MDBCol className="mt-2" >
                                                        <MDBBtn type="submit" color="warning" size="sm" onClick={this.confirmarEditar(2, 3, 4)} >Si</MDBBtn>
                                                        <MDBBtn color="primary" size="sm" onClick={this.toggle(3)}>No</MDBBtn>
                                                    </MDBCol>
                                                </MDBContainer>
                                            </MDBModalBody>
                                        </MDBModal>
                                    </form>
                                </MDBCol>
                            </MDBModalBody>
                        </MDBModal>
                    </div>
                </MDBCard>
                {/*Modal Info Usuario Editado */}
                <MDBModal size="sm" toggle={this.toggle(4)} isOpen={this.state.modal4} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#FBB138' }}><MDBIcon icon="exclamation-triangle" />   Usuario Editado Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                {/*Modal Info Usuario Eliminado */}
                <MDBModal toggle={this.toggle(6)} isOpen={this.state.modal6} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#C51E1E' }}><MDBIcon icon="user-times" />   Usuario Eliminado Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                <MDBModal size="sm" toggle={this.toggle('UsIngresado')} isOpen={this.state.modalUsIngresado} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#007E33' }}><MDBIcon icon="calendar-plus" />  Usuario Ingresado Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
            </Container>
        );
    }
}
export default SuperAdmin;