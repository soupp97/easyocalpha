import React, { Component } from 'react';
import { MDBIcon, MDBCard, MDBCardHeader, MDBCardBody, MDBBtn, MDBModal, MDBCol, MDBContainer, MDBInput, MDBModalBody, MDBRow, MDBModalHeader } from "mdbreact";
import { Table } from 'reactstrap';
import firebase from '../../../../Firebase';

class ProveedorCrud extends Component {
    servi = [];
    prov = [];
    constructor(props) {
        super(props);
        this.currUs = firebase.auth().currentUser.uid;
        this.ref = firebase.firestore().collection('servicio');
        this.ref2 = firebase.firestore().collection('proveedor');
        this.unsubscribe = null;
        this.state = {
            servicio: [],
            proveedor: [],
            rut_proveedor: '',
            jefe: '',
            razon_social: '',
            correo_contacto: '',
            rubro: '',
            fono: '',
            casa_central: '',
            web: '',
            nombre: '',
            descripcion: '',
            tiposervicio: '',
            cantidad_personas: '',
            costo: '',
            stock: '',
            exento: '',
            prov_uid: '',
            proved_key: '',
            serv_key: '',
            key: ''
        };
    }

    state = {
        rut_proveedorval: '',
        usr: '',
        modal1: false,
        modal2: false,
        modal3: false,
        modal4: false,
        modal5: false,
        modal6: false,
        modal7: false
    }

    verServ = (ce, tg) => () => {
        let modalName = 'modal' + tg;
        this.servi = ce;
        this.setState({
            [modalName]: !this.state[modalName],
            serv_key: this.servi.key
        });
        console.log(this.servi);
    }

    verProv = (ce, tg) => () => {
        let modalName = 'modal' + tg;
        this.prov = ce;
        this.setState({
            [modalName]: !this.state[modalName],
            proved_key: this.prov.key
        });
        console.log(this.servi);
    }
    confirmarEditar = (tg1, tg2, tg3) => () => {
        let modalName1 = 'modal' + tg1;
        let modalName2 = 'modal' + tg2;
        let modalName3 = 'modal' + tg3;
        this.setState({
            [modalName1]: !this.state[modalName1],
            [modalName2]: !this.state[modalName2],
            [modalName3]: !this.state[modalName3],
        })
    }

    eliminar = (usr1, tg) => () => {
        console.log(tg, usr1);
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            usr: usr1
        })
    }

    limpiarEdt = (tg) => () => {
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            nombre: '',
            descripcion: '',
            tiposervicio: '',
            cantidad_personas: '',
            costo: '',
            stock: '',
            exento: '',
            key: ''
        })
    }

    editarServicio = (usr1, tg) => () => {
        const ref2 = firebase.firestore().collection('servicio').doc(usr1);
        ref2.get().then((doc) => {
            if (doc.exists) {
                const user1 = doc.data();
                this.setState({
                    key: doc.id,
                    nombre: user1.nombre,
                    descripcion: user1.descripcion,
                    tiposervicio: user1.tiposervicio,
                    cantidad_personas: user1.cantidad_personas,
                    costo: user1.costo,
                    stock: user1.stock,
                    exento: user1.exento
                });
            } else {
                console.log("No such document!");
            }
        });
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
        })


    }

    toggle = nr => () => {
        console.log(nr);
        let modalName = 'modal' + nr;
        this.setState({
            [modalName]: !this.state[modalName]
        })
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    //Función para Agregar en Firebase
    onSubmit = (e) => {
        e.preventDefault();
        e.target.className += "was-validated";
        const { nombre, descripcion, tiposervicio, cantidad_personas, costo, stock, exento } = this.state;
        if (nombre.trim().length <= 0) {
            console.log("Debe ingresar un nombre");
        } else if (descripcion.trim().length <= 0) {
            console.log("Debe ingresar una descripción");
        } else if (tiposervicio.trim().length <= 0) {
            console.log("Debe ingresar un tipo de servicio");
        } else if (cantidad_personas.trim().length <= 0) {
            console.log("Debe ingresar una cantidad de personas");
        } else if (costo.trim().length <= 0) {
            console.log("Debe ingresar un costo");
        } else if (stock.trim().length <= 0) {
            console.log("Debe ingresar un stock");
        } else if (exento.trim().length <= 0) {
            console.log("Debe ingresar si está o no exento");
        } else {
            this.ref.add({
                nombre,
                descripcion,
                tiposervicio,
                cantidad_personas,
                costo,
                prov_uid : this.currUs,
                stock,
                exento
            }).then((docRef) => {
                this.setState({
                    nombre: '',
                    descripcion: '',
                    tiposervicio: '',
                    cantidad_personas: '',
                    costo: '',
                    stock: '',
                    exento: ''
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        }
    }

    //Función para Editar en Firebase
    onSubmit2 = (e) => {
        e.preventDefault();
        e.target.className += "was-validated";
        const { nombre, descripcion, tiposervicio, cantidad_personas, costo, stock, exento } = this.state;

        if (nombre.trim().length <= 0) {
            console.log("Debe ingresar un nombre");
        } else if (descripcion.trim().length <= 0) {
            console.log("Debe ingresar una descripción");
        } else if (tiposervicio.trim().length <= 0) {
            console.log("Debe ingresar un tipo de servicio");
        } else if (cantidad_personas.trim().length <= 0) {
            console.log("Debe ingresar una cantidad de personas");
        } else if (toString(costo).trim().length <= 0) {
            console.log("Debe ingresar un costo");
        } else if (stock.trim().length <= 0) {
            console.log("Debe ingresar un stock");
        } else if (exento.trim().length <= 0) {
            console.log("Debe ingresar si está o no exento");
        } else {
            const updateRef = firebase.firestore().collection('servicio').doc(this.state.key);
            updateRef.set({
                nombre,
                descripcion,
                tiposervicio,
                cantidad_personas,
                costo,
                stock,
                exento
            }).then((docRef) => {
                this.setState({
                    nombre: '',
                    descripcion: '',
                    tiposervicio: '',
                    cantidad_personas: '',
                    costo: '',
                    stock: '',
                    exento: ''
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        }
    }

    onCollectionUpdate = (querySnapshot) => {
        const servicio = [];
        querySnapshot.forEach((doc) => {
            const { nombre, descripcion, tiposervicio, prov_uid,cantidad_personas, costo, stock, exento } = doc.data();
            servicio.push({
                key: doc.id,
                doc, // DocumentSnapshot
                nombre,
                descripcion,
                tiposervicio,
                cantidad_personas,
                costo,
                prov_uid,
                stock,
                exento
            });
        });
        this.setState({
            servicio
        });
    }
    onCollectionUpdate2 = (querySnapshot) => {
        const proveedor = [];
        querySnapshot.forEach((doc) => {
            const { rut_proveedor, jefe, razon_social, correo_contacto, rubro, fono, casa_central, web } = doc.data();
            proveedor.push({
                key: doc.id,
                doc, // DocumentSnapshot
                rut_proveedor,
                jefe,
                razon_social,
                correo_contacto,
                rubro,
                fono,
                casa_central,
                web
            });
        });
        this.setState({
            proveedor
        });
    }
    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
        this.unsubscribe = this.ref2.onSnapshot(this.onCollectionUpdate2);
    }

    delete(id, tg, tg2) {
        firebase.firestore().collection('servicio').doc(id).delete().then(() => {
            console.log("Document successfully deleted!");
            this.props.history.push("#")

        }).catch((error) => {
            console.error("Error removing document: ", error);
        });
        let modalName = 'modal' + tg;
        let modalName2 = 'modal' + tg2;
        this.setState({
            [modalName]: !this.state[modalName],
            [modalName2]: !this.state[modalName2]
        })
    }
    editar = (usr1, tg) => () => {
        const ref2 = firebase.firestore().collection('proveedor').doc(usr1);
        ref2.get().then((doc) => {
            if (doc.exists) {
                const user1 = doc.data();
                this.setState({
                    key: doc.id,
                    rut_proveedor: user1.rut_proveedor,
                    jefe: user1.jefe,
                    razon_social: user1.razon_social,
                    correo_contacto: user1.correo_contacto,
                    rubro: user1.rubro,
                    fono: user1.fono,
                    casa_central: user1.casa_central,
                    web: user1.web
                });
            } else {
                console.log("No such document!");
            }
        });
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
        })
    }

    onSubmitEditar = (e) => {
        e.preventDefault();
        e.target.className += "was-validated";
        const { rut_proveedor, jefe, razon_social, correo_contacto, rubro, fono, casa_central, web } = this.state;
        const updateRef = firebase.firestore().collection('proveedor').doc(this.state.key);
        updateRef.set({
            rut_proveedor,
            jefe,
            razon_social,
            correo_contacto,
            rubro,
            fono,
            casa_central,
            web
        }).then((docRef) => {
            this.setState({
                key: '',
                rut_proveedor: '',
                jefe: '',
                razon_social: '',
                correo_contacto: '',
                rubro: '',
                fono: '',
                casa_central: '',
                web: ''
            });
            this.props.history.push("#")
        })
            .catch((error) => {
                console.error("Error adding document: ", error);
            });

    }


    render() {
        const { nombre, descripcion, tiposervicio, cantidad_personas, costo, stock, exento, usr, rut_proveedor, jefe, razon_social, correo_contacto, rubro, fono, casa_central, web } = this.state;
        return (

            <MDBContainer fluid>
                <MDBRow>
                    {/*  Columna con datos del proveedor */}
                    <MDBCol className="col-6">

                        {this.state.proveedor.map(proveedor => proveedor.key == this.currUs ?

                            <tr>
                                <MDBCard className="mb-2">
                                    <MDBCardHeader>
                                        <h2 style={{ textAlign: 'center' }}>{proveedor.razon_social}</h2>
                                    </MDBCardHeader>
                                    <MDBCardBody>
                                        <MDBRow>
                                            <MDBCol>
                                                <h6 style={{ textAlign: 'left', fontSize: '15px' }}>Rut:</h6>
                                                <h6 style={{ textAlign: 'left' }}>{proveedor.rut_proveedor}</h6>

                                            </MDBCol>
                                            <MDBCol>
                                                <h6 style={{ textAlign: 'left', fontSize: '15px' }}>Rubro:</h6>
                                                <h6 style={{ textAlign: 'left' }}>{proveedor.rubro}</h6>
                                            </MDBCol>

                                        </MDBRow>

                                    </MDBCardBody>
                                </MDBCard>
                                <MDBCard>
                                    <MDBCardHeader>
                                        <h2 style={{ textAlign: 'center' }}>Datos de Contacto</h2>
                                    </MDBCardHeader>
                                    <MDBCardBody>
                                    <MDBRow>
                                            <MDBCol>
                                            <h6 style={{ textAlign: 'left', fontSize: '20px' }}>Web</h6>
                                        <h6 style={{ textAlign: 'left' }}>{proveedor.web}</h6>
                                        <h6 style={{ textAlign: 'left', fontSize: '20px' }}>Jefe</h6>
                                        <h6 style={{ textAlign: 'left' }}>{proveedor.jefe}</h6>
                                        <h6 style={{ textAlign: 'left', fontSize: '20px' }}>Fono</h6>
                                        <h6 style={{ textAlign: 'left' }}>{proveedor.fono}</h6>

                                            </MDBCol>
                                            <MDBCol>
                                            <h6 style={{ textAlign: 'left', fontSize: '20px' }}>Correo Contacto</h6>
                                        <h6 style={{ textAlign: 'left' }}>{proveedor.correo_contacto}</h6>
                                        <h6 style={{ textAlign: 'left', fontSize: '20px' }}>Casa Central</h6>
                                        <h6 style={{ textAlign: 'left' }}>{proveedor.casa_central}</h6>
                                            </MDBCol>

                                        </MDBRow>
                                        
                                        
                                    </MDBCardBody>
                                </MDBCard>








                            </tr> : null

                        )}




                    </MDBCol>

                    {/* Columna con tabla de servicios de solo 1 proveedor */}
                    <MDBCol className="col-6">
                        <MDBCard>
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <MDBCardHeader><h3>Gestión de Servicios</h3></MDBCardHeader>
                                </div>
                                <Table bordered responsive striped>
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Tipo Servicio</th>
                                            <th>Cantidad de Personas</th>
                                            <th>Costo</th>
                                            <th>Stock</th>
                                            <th>Exento</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.servicio.map(servicio =>
                                        servicio.prov_uid == this.currUs ?

                                            <tr>
                                                <td>{servicio.nombre}</td>
                                                <td>{servicio.descripcion}</td>
                                                <td>{servicio.tiposervicio}</td>
                                                <td>{servicio.cantidad_personas}</td>
                                                <td>{servicio.costo}</td>
                                                <td>{servicio.stock}</td>
                                                <td>{servicio.exento}</td>
                                                <td scope="row">
                                                    <MDBBtn color="yellow" onClick={this.editarServicio(servicio.key, 2)} ><MDBIcon size="lg" icon="edit" /></MDBBtn>
                                                    <MDBBtn color="red" onClick={this.eliminar(servicio.key, 5)} ><MDBIcon size="lg" icon="times-circle" /></MDBBtn>
                                                </td>
                                            </tr> : null
                                        )}
                                    </tbody>
                                </Table>
                                </div>
                        </MDBCard>
                        <MDBModal size="md" toggle={this.toggle(5)} isOpen={this.state.modal5} backdrop={true}>
                            <MDBModalHeader>¿Desea eliminarlo?</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer fluid>
                                    <MDBRow middle="true" className="mb-5">
                                        <MDBCol>
                                            <h5 style={{ fontSize: '17px' }}>Esta acción es irreversible</h5>
                                        </MDBCol>
                                    </MDBRow>
                                    <MDBRow middle="true">
                                        <MDBCol className="mx-2">
                                            <MDBBtn color="red" size="md" onClick={this.delete.bind(this, usr, 5, 6)} >Si</MDBBtn>
                                        </MDBCol>
                                        <MDBCol className="mx-2">
                                            <MDBBtn color="blue" size="md" onClick={this.toggle(5)}>No</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>
                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>
                    </MDBCol>
                </MDBRow>
            <br />

                {/* Botón Editar Servicio */ }
        <MDBModal toggle={this.toggle(2)} isOpen={this.state.modal2} size="md" backdrop={false}>
            <MDBModalHeader>Ingrese los datos del Servicio</MDBModalHeader>
            <MDBModalBody >
                <MDBCol >
                    <form noValidate onSubmit={this.onSubmit2}>
                        <MDBRow>
                            <MDBRow middle="true">
                                <MDBCol>
                                    <MDBInput label="Nombre" outline className="form-control" name="nombre" value={nombre} onChange={this.onChange} ></MDBInput>
                                    <MDBInput label="Descripción" outline className="form-control" name="descripcion" value={descripcion} onChange={this.onChange} ></MDBInput>
                                    <MDBInput label="Tipo de Servicio" outline className="form-control" name="tiposervicio" onChange={this.onChange} value={tiposervicio} ></MDBInput>
                                    <MDBInput label="Cantidad de Personas" type="number" outline className="form-control" name="cantidad_personas" onChange={this.onChange} value={cantidad_personas}></MDBInput>
                                </MDBCol>
                                <MDBCol>
                                    <MDBInput label="Costo" type="number" outline className="form-control" name="costo" onChange={this.onChange} value={costo} ></MDBInput>
                                    <MDBInput label="Stock" type="number" outline className="form-control" name="stock" onChange={this.onChange} value={stock} ></MDBInput>
                                    <MDBInput label="Exento" outline className="form-control" name="exento" onChange={this.onChange} value={exento} ></MDBInput>
                                </MDBCol>
                            </MDBRow>
                        </MDBRow>
                        <MDBBtn color="primary" size="md" onClick={this.toggle(3)}>Editar</MDBBtn>
                        <MDBBtn color="secondary" size="md" onClick={this.limpiarEdt(2)}>Volver</MDBBtn>
                        {/*Sección Modal Confirmar Editar */}
                        <MDBModal className="pl-5" size="sm" toggle={this.toggle(3)} isOpen={this.state.modal3} backdrop={true}>
                            <MDBModalHeader>¿Está seguro?</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer center>
                                    ¿Está seguro que desea editar este Servicio?
                                                    <br></br>
                                    <br></br>
                                    <MDBCol >
                                        <MDBBtn type="submit" color="red" size="md" onClick={this.confirmarEditar(2, 3, 4)} >Si</MDBBtn>
                                        <MDBBtn color="blue" size="md" onClick={this.toggle(3)}>No</MDBBtn>
                                    </MDBCol>
                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>

                    </form>
                </MDBCol>
            </MDBModalBody>
        </MDBModal>

        {/* Fila columnas con botones ubicados al final de la interfaz Dashboard del Proveedor */ }
        <MDBRow>

            {/* Botón Editar Proveedor */}

            <MDBModal toggle={this.toggle(7)} isOpen={this.state.modal7} size="md" backdrop={false}>
                <MDBModalHeader>Ingrese los datos del Proveedor</MDBModalHeader>
                <MDBModalBody >
                    <MDBCol >
                        <form noValidate onSubmit={this.onSubmitEditar}>
                            <MDBRow>
                                <MDBCol>
                                    <div className="form-group">
                                        <label htmlFor="rut_proveedor">Rut proveedor:</label>
                                        <input required className="form-control" name="rut_proveedor" value={rut_proveedor} onChange={this.onChange} placeholder="Rut proveedor" ></input>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="jefe">Jefe:</label>
                                        <input required className="form-control" name="jefe" onChange={this.onChange} value={jefe} placeholder="Nombres" ></input>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="razon_social">Razon social:</label>
                                        <input required className="form-control" name="razon_social" onChange={this.onChange} value={razon_social} placeholder="Razon social" ></input>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="correo_contacto">Correo contacto:</label>
                                        <input required className="form-control" name="correo_contacto" type="email" onChange={this.onChange} value={correo_contacto} placeholder="Correo contacto" ></input>
                                    </div>
                                    

                                    

                                </MDBCol>
                                <MDBCol>
                                <div className="form-group">
                                        <label htmlFor="rut_proveedor">Fono</label>
                                        <input required className="form-control" name="fono" onChange={this.onChange} value={fono} placeholder="Fono" ></input>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="casa_central">Casa central:</label>
                                        <input required className="form-control" name="casa_central" onChange={this.onChange} value={casa_central} placeholder="Casa central" ></input>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="web">Web:</label>
                                        <input required className="form-control" name="web" onChange={this.onChange} value={web} placeholder="Web" ></input>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="rubro">Rubro:</label>
                                        <input required className="form-control" name="rubro" onChange={this.onChange} value={rubro} placeholder="Rubro" ></input>
                                    </div>
                                </MDBCol>

                            </MDBRow>
                            {/*Botones Agregar Proveedor */}
                            <MDBRow>
                                <MDBCol className="text-center">
                                    <MDBBtn color="warning" size="md" onClick={this.toggle(3)}>Editar</MDBBtn>
                                    <MDBBtn color="primary" size="md" onClick={this.toggle(7)}>Volver</MDBBtn>
                                </MDBCol>
                            </MDBRow>
                            <MDBRow>
                                <MDBCol className="text-center">
                                    {this.state.rut_proveedorval}
                                </MDBCol>
                            </MDBRow>
                            {/*Sección Modal Confirmar Editar */}
                            <MDBModal className="pl-5" size="sm" toggle={this.toggle(3)} isOpen={this.state.modal3} backdrop={true}>
                                <MDBModalHeader>¿Está seguro?</MDBModalHeader>
                                <MDBModalBody>
                                    <MDBContainer center>
                                        Está seguro que desea editar este Proveedor?
                                                    <br></br>
                                        <br></br>
                                        <MDBCol >
                                            <MDBBtn type="submit" color="warning" size="md" onClick={this.confirmarEditar(3, 4)} >Si</MDBBtn>
                                            <MDBBtn color="primary" size="md" onClick={this.toggle(3)}>No</MDBBtn>
                                        </MDBCol>
                                    </MDBContainer>
                                </MDBModalBody>
                            </MDBModal>

                        </form>
                    </MDBCol>
                </MDBModalBody>
            </MDBModal>
            {/* Botón Ver editar prov */}
            <MDBCol>
                    <MDBBtn size="md" md="4" block onClick={this.editar(this.currUs, 7)} color="primary">Editar Datos Proveedor</MDBBtn>
                
            </MDBCol>
            {/* Botón Ver Solicitudes */}
            <MDBCol>
                <MDBBtn size="md" md="4" block color="primary" href="/proveedorsoli">Ver Solicitudes</MDBBtn>
            </MDBCol>

            {/* Botón Agregar Servicio */}
            <MDBCol>
                <MDBBtn size="md" md="4" block color="primary" onClick={this.toggle(1)}>Nuevo Servicio</MDBBtn>

                <MDBModal toggle={this.toggle(1)} isOpen={this.state.modal1} size="md" backdrop={false}>
                    <MDBModalHeader>Ingrese los datos del nuevo servicio por favor</MDBModalHeader>
                    <MDBModalBody >
                        <MDBContainer>
                            <MDBRow middle="true">
                                <MDBCol >
                                    <form onSubmit={this.onSubmit}>
                                        <MDBRow middle="true">
                                            <MDBCol>
                                                <MDBInput label="Nombre" outline className="form-control" name="nombre" value={nombre} onChange={this.onChange} ></MDBInput>
                                                <MDBInput label="Descripción" outline className="form-control" name="descripcion" value={descripcion} onChange={this.onChange} ></MDBInput>
                                                <MDBInput label="Tipo de Servicio" outline className="form-control" name="tiposervicio" onChange={this.onChange} value={tiposervicio} ></MDBInput>
                                                <MDBInput label="Cantidad de Personas" type="number" outline className="form-control" name="cantidad_personas" onChange={this.onChange} value={cantidad_personas}></MDBInput>
                                            </MDBCol>
                                            <MDBCol>
                                                <MDBInput label="Costo" type="number" outline className="form-control" name="costo" onChange={this.onChange} value={costo} ></MDBInput>
                                                <MDBInput label="Stock" type="number" outline className="form-control" name="stock" onChange={this.onChange} value={stock} ></MDBInput>
                                                <MDBInput label="Exento" outline className="form-control" name="exento" onChange={this.onChange} value={exento} ></MDBInput>
                                            </MDBCol>
                                        </MDBRow>

                                    
                                    <MDBBtn type="submit" color="primary" size="md" onClick={this.toggle(1)}>Agregar</MDBBtn>
                                    <MDBBtn color="secondary" size="md" onClick={this.toggle(1)}>Volver</MDBBtn>
                                    </form>
                                </MDBCol>
                            </MDBRow>
                        </MDBContainer>


                    </MDBModalBody>
                </MDBModal>
            </MDBCol>

        </MDBRow>

            <MDBModal size="sm" toggle={this.toggle(4)} isOpen={this.state.modal4} position="top" frame>
                <MDBModalBody className="text-center">
                    <p style={{ color: '#FBB138' }}><MDBIcon icon="exclamation-triangle" />   Servicio Editado Satisfactoriamente</p>
                </MDBModalBody>
            </MDBModal>
            <MDBModal toggle={this.toggle(6)} isOpen={this.state.modal6} position="top" frame>
                <MDBModalBody className="text-center">
                    <p style={{ color: '#C51E1E' }}><MDBIcon icon="user-times" />   Servicio Eliminado Satisfactoriamente</p>
                </MDBModalBody>
            </MDBModal>


            </MDBContainer >

        );
    }
}

export default ProveedorCrud;