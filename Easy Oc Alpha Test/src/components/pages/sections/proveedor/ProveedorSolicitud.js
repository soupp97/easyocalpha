import React, { Component } from 'react';
import { MDBDataTable, MDBCard, MDBCardHeader, MDBCardText, MDBTable, MDBTableBody, MDBCardBody, MDBIcon, MDBBtn, MDBModal, MDBCol, MDBContainer, MDBInput, MDBModalBody, MDBRow, MDBModalHeader } from "mdbreact";
import { Table, Container } from 'reactstrap';
import firebase from '../../../../Firebase';
import "react-datepicker/dist/react-datepicker.css";
import { formatDate } from 'tough-cookie';
import moment from 'moment';

class ProveedorSolicitud extends Component {

    constructor(props) {
        super(props);
        this.ref = firebase.firestore().collection('solicitud');
        this.unsubscribe = null;
        this.state = {
            solicitud: [],
            //vigencia: '',
            //archivo: '',
            num_sol: '',
            desc_sol: '',
            tipo_sol: '',
            area_sol: '',
            estado: '',
            fecha_emision: '',
            fecha_vence: '',
            rut_us_sol: '',
            nom_us_sol: '',
            tipo_problema: '',
            key: ''
        };

    }

    state = {
        usr: '',
        modal1: false //subir factura
    }

    toggle = nr => () => { //función para que aparezca y desaparezca el modal
        let modalName = 'modal' + nr;
        this.setState({
            [modalName]: !this.state[modalName]
        })
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }


    onCollectionUpdate = (querySnapshot) => {
        const solicitud = [];
        querySnapshot.forEach((doc) => {
            const { num_sol, desc_sol, tipo_sol, area_sol, estado, fecha_emision, fecha_vence, rut_us_sol, nom_us_sol, tipo_problema } = doc.data();
            solicitud.push({
                key: doc.id,
                doc, // DocumentSnapshot
                num_sol,
                desc_sol,
                tipo_sol,
                area_sol,
                estado,
                fecha_emision,
                fecha_vence,
                rut_us_sol,
                nom_us_sol,
                tipo_problema

            });
        });
        this.setState({
            solicitud
        });
    }


    render() {
        const { num_sol, desc_sol, tipo_sol, area_sol, estado, fecha_emision, fecha_vence, rut_us_sol, nom_us_sol, tipo_problema } = this.state;
        return (

            <MDBCard>
                <MDBCardText>
                    <h2 style={{ textAlign: 'center' }}><strong>Solicitudes</strong></h2>
                </MDBCardText>
                <MDBCardBody >
                    <div className="scrollbar scrollbar-primary mx-auto" style={{ maxHeight: '350px' }}>
                        {/* --Lista de Solicitudes de OC para Proveedor-- */}
                        <MDBTable hover ScrollY searching={true} >
                            <MDBTableBody >
                                {this.state.solicitud.map(solicitud =>
                                    <MDBCard className="p-2 m-1">
                                        <MDBRow className="text-left mx-1">
                                            <MDBCol className="m-1 p-1">
                                                <h6 className="listaSolProv"><MDBIcon far icon="calendar" size="2" /> {solicitud.num_sol}</h6>
                                                <h6 className="listaSolProv"><MDBIcon far icon="calendar-alt" size="2" /> {moment(solicitud.fecha_emision.toDate()).format('DD/MM/YYYY').toString()}</h6>
                                            </MDBCol>
                                            <MDBCol className="m-1 p-1">
                                                <h6 className="listaSolProv"><MDBIcon icon="user" size="2" /> {solicitud.nom_us_sol}</h6>
                                                <h6 className="listaSolProv"><MDBIcon icon="arrow-down" size="2" /> OC_1723.pdf</h6> {/* Archivo OC */}
                                            </MDBCol>
                                            <MDBCol middle="true">
                                                <MDBBtn className="mb-2" block color="primary" size="md">Descargar Orden de Compra</MDBBtn>
                                            </MDBCol>
                                            <MDBCol middle="true" >
                                                <MDBBtn className="mb-2" block color="primary" size="md" onClick={this.toggle(1)}>Subir Factura</MDBBtn>



                                            </MDBCol>
                                        </MDBRow>
                                    </MDBCard>
                                )
                                }
                                <MDBModal size="md" toggle={this.toggle(1)} isOpen={this.state.modal1} backdrop={true}>
                                    <MDBModalHeader>Subir Factura</MDBModalHeader>
                                    <MDBModalBody>
                                        <MDBContainer fluid>
                                            <MDBRow middle="true" className="mb-5">
                                                <MDBCol>
                                                    <h5 style={{ fontSize: '17px' }}>Subir factura de la solicitud en formato en pdf</h5>
                                                </MDBCol>
                                            </MDBRow>
                                            <MDBRow >
                                                {/* Subir pdf  */}
                                                <input  type="file" />
                                            </MDBRow>
                                            <br />
                                            <br/>
                                            <MDBRow middle="true">
                                                <MDBCol className="mx-2">
                                                    <MDBBtn color="blue" block size="md">Subir Factura</MDBBtn>
                                                </MDBCol>
                                                <MDBCol className="mx-2">
                                                    <MDBBtn color="blue" block size="md" onClick={this.toggle(1)}>Salir</MDBBtn>
                                                </MDBCol>
                                            </MDBRow>
                                        </MDBContainer>
                                    </MDBModalBody>
                                </MDBModal>

                            </MDBTableBody>
                        </MDBTable>
                    </div>
                </MDBCardBody>
            </MDBCard>


        )
    }
}

export default ProveedorSolicitud;