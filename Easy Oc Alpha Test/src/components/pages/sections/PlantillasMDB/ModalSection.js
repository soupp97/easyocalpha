import React, { Component } from 'react';
import { MDBCol, MDBRow, MDBIcon, MDBCardBody, MDBCardHeader,MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';

class ModalSection extends Component {
  state = {
    modal1: false,
    modal2: false,
    modal3: false,
    modal4: false,
    modal5: false,
    modal6: false,
    modal7: false,
    modal8: false,
    modal9: false,
    modal10: false,
    modal11: false,
    modal12: false,
    modal13: false
  }

  toggle = nr => () => {
    let modalName = 'modal' + nr;
    this.setState({
      [modalName]: !this.state[modalName]
    })
  }

  render() {
    return (
        <MDBRow className="div-botones">
          <MDBCol>
              <MDBBtn size="lg" block color="primary" onClick={this.toggle(1)}><MDBIcon size="lg" icon="plus-circle"/>  Solicitud</MDBBtn>
              
              <MDBModal toggle={this.toggle(1)} isOpen={this.state.modal1} position="top" frame>
                <MDBModalBody className="text-center">
                  <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit nisi quo provident fugiat reprehenderit nostrum quos...</span>
                  <MDBBtn color="secondary" onClick={this.toggle(1)}>Close</MDBBtn>
                  <MDBBtn color="primary" onClick={this.toggle(1)}>Save changes</MDBBtn>
                </MDBModalBody>
              </MDBModal>

          </MDBCol>
          <MDBCol >
            <MDBBtn size="lg" block color="primary"  onClick={this.toggle(2)}> Reportes</MDBBtn>
            <MDBModal toggle={this.toggle(2)} isOpen={this.state.modal2} position="bottom" frame>
              <MDBModalBody className="text-center">
                <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit nisi quo provident fugiat reprehenderit nostrum quos...</span>
                <MDBBtn color="secondary" onClick={this.toggle(2)}>Close</MDBBtn>
                <MDBBtn color="primary" onClick={this.toggle(2)}>Save changes</MDBBtn>
              </MDBModalBody>
            </MDBModal>

          

          </MDBCol>

          <MDBCol >
              <MDBBtn size="lg" block color="primary"  onClick={this.toggle(9)}>Administración</MDBBtn>
              <MDBModal toggle={this.toggle(9)} isOpen={this.state.modal9} size="lg">
                <MDBModalHeader>Modal Title</MDBModalHeader>
                <MDBModalBody className="text-center">
                  <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit nisi quo provident fugiat reprehenderit nostrum quos...</span>
                  <MDBBtn color="secondary" onClick={this.toggle(9)}>Close</MDBBtn>
                </MDBModalBody>
              </MDBModal>

          </MDBCol>
        </MDBRow>
      
    )
  }
}

export default ModalSection;