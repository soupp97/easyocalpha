import { MDBBtn, MDBInput, MDBFormInline, MDBModalHeader, MDBCard, MDBCardBody, MDBCardHeader, MDBCardTitle, MDBTable, MDBTableBody, MDBCol, MDBContainer, MDBIcon, MDBModal, MDBModalBody, MDBRow } from 'mdbreact';
import React, { Component } from 'react';
import firebase from '../../../../Firebase';
import { getTime, getDate } from 'date-fns/esm';
import NumberFormat from 'react-number-format'; //Libreria para formatear presupuesto



class FormNuevaSolSection extends Component {
  constructor(props) {
    super(props);
    this.currUs = firebase.auth().currentUser.uid;
    this.refProveedor = firebase.firestore().collection('proveedor');
    this.refBit = firebase.firestore().collection('bitacora');
    this.refServicio = firebase.firestore().collection('servicio');
    this.refCECO = firebase.firestore().collection('centro_costo');
    this.refArea = firebase.firestore().collection('area');
    this.refUs = firebase.firestore().collection('usuario');
    this.ref = firebase.firestore().collection('solicitud');
    this.refCta = firebase.firestore().collection('cuenta');
    this.refCot = firebase.firestore().collection('cotizacion');
    this.unsubscribe = null;
    this.state = {
      solicitud: [],
      cotizacion: [],
      area: [],
      cuenta: [],
      usuario: [],
      CECO: [],
      IsAsignada: null,//Variable para reconocer cotizaciones sin asignar a Solicitud
      CECOus: '',
      ceco_key: '',
      nombre_cta: '',
      nro_cuenta: '',
      nm_area: '',
      area_sol: '',
      desc_sol: '',
      estado: '',
      oco: '',
      estadoVal: '',
      fecha_emision: '',
      prov_uid: '',//Variable para rescatar la uid del Proveedor
      fecha_vence: '',
      tipo_sol: '',
      key: '',
      costo: '',
      nombre: '',
      nro_ceco: '',
      presupuesto: '',
      razón_social: '',
      exento: '',
      nro_cta: '',
      nm_proveedor: '',
      nm_servicio: '',
      total_cot: '',
      usuKey: '',
      Cotz: {},
      servicio: [],
      proveedor: [],
      //Datos de Bitácora
      uid_sol: '',
      fecha: '',
      n_us: '',
      estadoSol: '',
      cotPref:'',//Variable para almacenar la cotización preferida
      HayCot: false,//Variable para revisar si hay cotizaciones seleccionadas
      HayCot2: null//Variable para revisar si hay cotizaciones

    }
  }

  state = {
    modalCot: false,
    cotSel: false,
    modalSolIngresada: false,
    modalSolIngreseCot: false,
    modalSolSeleccioneCot: false,
    modalMax3Cot: false,
    slc_serv: '',
    msg_desc_ta: '',
    radio: '',

    data: []
  }
  //Funcion para usar el checkbox
  onClick = (nr) => () => {
    this.setState({
      radio: nr
    });
    this.setState({
      HayCot: true
    });
  }
  //Función para usar modal
  toggle = nr => () => {
    let modalName = 'modal' + nr;
    this.setState({
      [modalName]: !this.state[modalName]
    })
  }
  //Función para leer los cambios de un componente
  onChange = (e) => {
    e.preventDefault();
    const state = this.state
    //Función para validar comentarios max 10 carácteres
    if (e.target.name == 'desc_sol') {
      e.target.value.trim().length < 11 ?
        this.setState({ msg_desc_ta: <p style={{ color: 'red' }} id="desc_sol" class="form-text ">Mínimo 10 carácteres.</p> }) :
        this.setState({ msg_desc_ta: <small id="desc_sol" class="form-text text-muted">Mínimo 10 carácteres.</small> });
    } else {

    }
    state[e.target.name] = e.target.value
    this.setState(state);
  }
  //Función para leer los cambios del Proveedor y actualizar los productos según uid del Proveedor
  OnChangeProv = (e) => {
    const state = this.state
    this.prov_uid = e.target.value
    this.state.proveedor.map(prov => {
      if (this.prov_uid == prov.key) {
        state[e.target.name] = prov.razon_social
      }
    })
    this.setState(state);
  }

  componentDidMount() {
    this.unsubscribe = this.refServicio.onSnapshot(this.onCollectionUpdate);
    this.unsubscribe = this.refProveedor.onSnapshot(this.onCollectionUpdate2);
    this.unsubscribe = this.refCot.onSnapshot(this.onCollectionUpdate3);
    this.unsubscribe = this.refArea.onSnapshot(this.onCollectionUpdate4);
    this.unsubscribe = this.refUs.onSnapshot(this.onCollectionUpdate5);
    this.unsubscribe = this.refCta.onSnapshot(this.onCollectionUpdate6);
    this.unsubscribe = this.refCECO.onSnapshot(this.onCollectionUpdate7);
  }
  //Función para leer los cambios del Servicio y actualizar los servicios según uid del Proveedor
  onChangeServ = (e) => {
    const state = this.state
    this.state.servicio.map(serv => {
      if (serv.key == e.target.value) {
        this.costo = serv.costo;
        state[e.target.name] = serv.nombre;
      }
    })
    this.setState(state);
    this.setState({ costo: this.costo });
  }
  //Función para leer los cambios del proveedor
  onChangeCot = (e) => {
    const state = this.state
    this.nm_proveedor = e.target.value
    state[e.target.name] = e.target.value
    this.setState(state);
  }

  onCollectionUpdate2 = (querySnapshot) => {
    const proveedor = [];
    querySnapshot.forEach((doc) => {
      const { razon_social } = doc.data();
      proveedor.push({
        key: doc.id,
        doc, // DocumentSnapshot
        razon_social
      });
    });
    this.setState({
      proveedor
    });

  }

  onCollectionUpdate7 = (querySnapshot) => {
    const CECO = [];
    querySnapshot.forEach((doc) => {
      const { nombre, presupuesto, nro_ceco } = doc.data();
      CECO.push({
        key: doc.id,
        doc, // DocumentSnapshot
        nombre,
        presupuesto,
        nro_ceco
      });
    });
    this.setState({
      CECO
    });

  }

  onCollectionUpdate3 = (querySnapshot) => {
    const cotizacion = [];
    querySnapshot.forEach((doc) => {
      const { nm_proveedor, nm_servicio, costo, usuKey, IsAsignada } = doc.data();
      cotizacion.push({
        key: doc.id,
        doc, // DocumentSnapshot
        nm_proveedor,
        usuKey,
        IsAsignada,
        nm_servicio,
        costo
      });
    });
    this.setState({
      cotizacion
    });

  }

  onCollectionUpdate4 = (querySnapshot) => {
    const area = [];
    querySnapshot.forEach((doc) => {
      const { nm_area } = doc.data();
      area.push({
        key: doc.id,
        doc, // DocumentSnapshot
        nm_area,
      });
    });
    this.setState({
      area
    });

  }

  onCollectionUpdate5 = (querySnapshot) => {
    const usuario = [];
    querySnapshot.forEach((doc) => {
      const { CECOus } = doc.data();
      usuario.push({
        key: doc.id,
        doc, // DocumentSnapshot
        CECOus
      });
    });
    this.setState({
      usuario
    });


  }

  onCollectionUpdate6 = (querySnapshot) => {
    const cuenta = [];
    querySnapshot.forEach((doc) => {
      const { ceco_key, nro_cuenta, nombre_cta } = doc.data();
      cuenta.push({
        key: doc.id,
        doc, // DocumentSnapshot
        ceco_key,
        nro_cuenta,
        nombre_cta
      });
    });
    this.setState({
      cuenta
    });

  }

  onCollectionUpdate = (querySnapshot) => {
    const servicio = [];
    querySnapshot.forEach((doc) => {
      const { nombre, costo, exento, prov_uid } = doc.data();
      servicio.push({
        key: doc.id,
        doc, // DocumentSnapshot
        nombre,
        costo,
        prov_uid,
        exento
      });
    });
    this.setState({
      servicio
    });
  }

  /*Sección Agregar Solicitud */
  onSubmit = (e) => {
    e.preventDefault();
    e.target.className += "was-validated";
    const Cotz = [];
    let total_sol = 0;

    const { area_sol, desc_sol, radio, oco, cotPref, n_us, fecha_vence, nro_cta, tipo_sol } = this.state;
    if (area_sol.trim().length <= 0) {
    } else if (desc_sol.trim().length < 10) {
    } else if (nro_cta.trim().length < 0 || nro_cta == "Seleccione Cuenta") {

    } else {

      for (let index = 0; index < this.state.cotizacion.length; index++) {
        const cot = this.state.cotizacion[index];
        if (cot.usuKey == this.currUs) {
          if (cot.IsAsignada == false) {
            this.setState({ HayCot2: true });//Si hay almenos una cotizacion hace trigger esta variable
            if (this.state.radio == cot.key) {
              this.setState({ cotPref: cot.key });
              const refCo = firebase.firestore().collection('cotizacion').doc(cot.key);
              total_sol = cot.costo
              refCo.update({
                IsAsignada: true
              });
            }
            if (this.state.HayCot == true) {
              Cotz.push({ cotKey: cot.key });
              this.setState({ Cotz });
              const refCo = firebase.firestore().collection('cotizacion').doc(cot.key);
              refCo.update({
                IsAsignada: true
              });
            } 
          } 

        }
      }
      if (this.state.HayCot2 == false) {
          let modalName = 'modalSolIngreseCot';
          this.setState({
            [modalName]: !this.state[modalName]
          });
      } else {
        if (total_sol > 0) {
          this.ref.add({
            area_sol,
            desc_sol,
            estado: 'Nueva',
            estadoVal: 'Por Validar',
            oco,
            nro_cta,
            usuKey: this.currUs,
            fecha_emision: Date.now(),
            fecha_vence,
            tipo_sol,
            codSap:'No Asignado',
            total_sol,
            cotPref : radio,
            Cotz
          }).then((docRef) => {
            this.setState({
              area_sol: '',
              desc_sol: '',
              estado: '',
              oco: '',
              fecha_emision: '',
              estadoVal: '',
              fecha_vence: '',
              tipo_problema: '',
              tipo_sol: '',
              nro_cta: '',
              cotPref:'',
              msg_desc_ta: null,
              Cotz
            });
            let modalName = 'modalSolIngresada';
            this.setState({
              [modalName]: !this.state[modalName]
            })
            let nmus = '';

            //Agrega Bitacora de Solicitud
            this.refBit.add({
              uid_sol: docRef.id,
              fecha: Date.now(),
              n_us: this.currUs,
              estadoSol: 'Solicitud Creada',
              comentario: 'Solicitud Creada'
            }).then((docRef) => {
              this.setState({
                uid_sol: '',
                fecha: '',
                n_us: '',
                estadoSol: ''
              })
            });

            this.props.history.push("/")
          })
            .catch((error) => {
              console.error("Error adding document: ", error);
            });

        } else {//Si no hay cotizaciones seleccionadas levanta el modal
          let modalName = 'modalSolSeleccioneCot';
          this.setState({
            [modalName]: !this.state[modalName]
          })
        }
      } 

    }
  }

  /*Sección Agregar Cotización */
  onSubmit2 = (e) => {
    e.preventDefault();
    e.target.className += "was-validated";
    let cantCot = 1; //Variable para validar que no se pueda seguir agregando más de 3 cotizaciones
    const currUs = firebase.auth().currentUser.uid;//Variable que recoje el uid de la sesión actual
    const { nm_proveedor, nm_servicio, costo, usuKey } = this.state;
    this.state.cotizacion.map(cot => cot.usuKey == currUs ? cot.IsAsignada == true ? null : cantCot += 1 : null);
    console.log(cantCot);
    if (cantCot > 3) {
      let modalName = 'modalMax3Cot';
      this.setState({
        [modalName]: !this.state[modalName]
      })

    } else if (nm_proveedor.trim().length <= 0) {

    } else if (nm_servicio.trim().length <= 0) {

    } else if (costo.length <= 0) {

    } else {
      this.refCot.add({
        nm_proveedor,
        nm_servicio,
        usuKey: currUs,
        IsAsignada: false,
        costo
      }).then((docRefCot) => {
        this.setState({
          nombre_prov: '',
          nm_servicio: '',
          costo: ''
        });

      })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
    }
  }


  /**Sección Eliminar Cotización */
  deleteCot(id) {
    firebase.firestore().collection('cotizacion').doc(id).delete().then(() => {
      firebase.auth()
      console.log("Document successfully deleted!");
      this.props.history.push("#")

    }).catch((error) => {
      console.error("Error removing document: ", error);
    });
  }

  render() {
    const { area_sol, desc_sol, oco, estado, IsAsignada, fecha_emision, fecha_vence, num_sol, rut_us_sol, nro_cta, tipo_sol } = this.state;
    return (
      <MDBCard>
        <MDBContainer fluid>

          {/*-Fila principal Ingresar Solicitud */}
          <MDBRow >
            {/*-Sección Datos Solicitud */}

            <MDBCol size="md-6">
              <MDBCardBody>
                <form noValidate onSubmit={this.onSubmit}>

                  <h2>Nueva Solicitud</h2>
                  <hr></hr>
                  <MDBRow>
                    {/*-Sección iconos */}
                    <MDBCol size="mb-1 pt-2">
                      <MDBIcon icon="calendar-plus" size="lg" />
                    </MDBCol>
                    {/*-Sección Input */}

                    <MDBCol className="mb-2">
                      <div>
                        <select required name="tipo_sol" value={tipo_sol} onChange={this.onChange} className="browser-default custom-select col-lg-6" id="TSolicitud">
                          <option>Seleccione Tipo de Solicitud</option>
                          <option value="Orden de compra">Orden de compra</option>
                          <option value="Suma por rendir" disabled='true'>Suma por rendir</option>
                          <option value="Reembolso" disabled='true'>Reembolso</option>
                        </select>
                      </div>
                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    {/*-Sección iconos */}
                    <MDBCol size="mb-1 pt-2">
                      <MDBIcon icon="exclamation-triangle" size="md" />
                    </MDBCol>
                    {/*-Sección Input */}
                    <MDBCol className="mb-2">
                      <select required name="area_sol" value={area_sol} onChange={this.onChange} className="browser-default custom-select col-lg-6" id="area_sol">
                        <option>Seleccione el Area</option>
                        {this.state.area.map(ar =>
                          <option value={ar.nm_area}>{ar.nm_area}</option>)}
                      </select>


                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    {/*-Sección iconos */}
                    <MDBCol size="mb-2 pt-4" className="ml-3">
                    </MDBCol>
                    {/*-Sección Input */}
                    <MDBCol className="mb-2">
                      <textarea required className="form-control" name="desc_sol" onChange={this.onChange} value={desc_sol} placeholder="Descripción" id="desc_sol_ta" rows="4" />
                      {this.state.msg_desc_ta}
                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    {/*-Sección iconos */}
                    <MDBCol size="mb-1 pt-2">
                      <MDBIcon icon="key" size="lg" />
                    </MDBCol>
                    {/*-Sección Input Cuenta */}
                    <MDBCol className="mb-2 pl-2">
                      <select name="nro_cta" value={nro_cta} onChange={this.onChange} className="browser-default custom-select col-lg-6" id="nro_cta">
                        <option value=" ">Seleccione Cuenta</option>
                        {this.state.usuario.map(us => us.key == this.currUs ?
                          this.state.cuenta.map(cta => cta.ceco_key == us.CECOus ?
                            <option value={cta.key}>{cta.nombre_cta + ' ' + cta.nro_cuenta}</option> : null) : null)}
                      </select>

                    </MDBCol>
                  </MDBRow>
                  <MDBRow>

                    {/*-Sección Input */}
                    <MDBCol className="mb-2 ml-3 col-lg-6">
                      <input type="int" placeholder="Ingrese OCO (Opcional)" requider className="form-control" name="oco" onChange={this.onChange} value={oco} type="text" />

                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    <MDBCol className="text-center">
                      <MDBBtn type="submit" color="green" size="md" >Agregar</MDBBtn>
                    </MDBCol>
                  </MDBRow>
                </form>

              </MDBCardBody>
            </MDBCol>
            {/*-Sección Datos Cotización */}

            <MDBCol size="md-6">
              <MDBCardBody>
                <h2>Cotización</h2>
                <hr></hr>
                <MDBRow>
                  {/*Botón Agregar Cotización*/}
                  <MDBCol className="text-center">
                    <MDBBtn size="sm" className="mb-6" color="green" onClick={this.toggle('Cot')}>
                      <h6 style={{ fontSize: '14px' }} className="mt-1">Añadir Cotización <MDBIcon size="lg" className="pl-3" icon="plus-square" /></h6>
                    </MDBBtn>
                  </MDBCol>
                </MDBRow>
              </MDBCardBody>
              {/*Sección Mostrar Cotización */}
              <MDBCardBody>
                <div className="scrollbar scrollbar-primary mx-auto" style={{ maxHeight: '350px' }}>
                  <fieldset className="form-group">
                    {this.state.cotizacion.map(coti =>
                      coti.IsAsignada == false ?
                        coti.usuKey == this.currUs ?
                          <MDBCard className="p-2 m-1">
                            <MDBRow className="text-left mx-1">
                              <MDBCol>
                                <MDBFormInline>
                                  <h4 style={{ fontSize: '14px' }}> {coti.nm_proveedor} <MDBIcon className="px-1" icon="angle-right" /> </h4>
                                  <h4 style={{ fontSize: '14px' }}> {coti.nm_servicio}  <MDBIcon className="px-1" icon="angle-right" /></h4>
                                  <h4 style={{ fontSize: '14px' }}> <NumberFormat value={coti.costo} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h4>
                                  <MDBCol className="text-right">

                                    <h4 style={{ fontSize: '14px' }}> <MDBBtn onClick={this.deleteCot.bind(this, coti.key)} style={{ borderRadius: '25%' }} color="red" className="p-1"><MDBIcon size="1x" icon="times" className="m-1" /></MDBBtn> </h4>
                                  </MDBCol>
                                  <MDBInput gap size="5" onClick={this.onClick(coti.key)} checked={this.state.radio === coti.key ? true : false} type="radio"
                                    id={coti.key} />

                                </MDBFormInline>
                              </MDBCol>
                            </MDBRow>
                          </MDBCard> : null : null)
                    }
                  </fieldset>
                </div>
              </MDBCardBody>
              {/*Sección Modal Agregar Cotización */}
            </MDBCol>
            <MDBModal size="md" isOpen={this.state.modalCot} toggle={this.toggle()}>
              <form noValidate onSubmit={this.onSubmit2}>
                <MDBCardHeader><h4>Añadir nueva Cotización</h4></MDBCardHeader>
                <MDBModalBody>
                  <MDBRow>

                    <MDBCol className="col-6">
                      <div>
                        <div className="form-label">
                          <label htmlFor="nm_proveedor">Proveedor:</label>
                        </div>
                        {/*Sección Seleccionar Proveedor */}
                        <select required name="nm_proveedor" onChange={this.OnChangeProv} className="browser-default custom-select" id="nm_proveedor">
                          <option>Seleccione Proveedor</option>
                          {this.state.proveedor.map(proveedor =>
                            <option value={proveedor.key}>{proveedor.razon_social}</option>
                          )}
                        </select>
                      </div>
                    </MDBCol>
                    <MDBCol className="col-6">
                      <div className="form-label">
                        <label className="form-label" htmlFor="nm_servicio">Servicio:</label>
                      </div>
                      {/*Sección Seleccionar Servicio*/}
                      <select onChange={this.onChangeServ} name="nm_servicio" className="browser-default custom-select">
                        <option value='0' >Seleccione Servicio:</option>
                        {this.state.servicio.map(servicio =>
                          servicio.prov_uid == this.prov_uid ?
                            <option value={servicio.key}>{servicio.nombre}</option> : (this.null)
                        )}
                      </select>
                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    <MDBCol>
                      {/* Actualiza total según el precio*/}
                      <MDBCard className="my-3">
                        <MDBCardHeader>Total: </MDBCardHeader>
                        <MDBCardBody ><MDBIcon size="md" className="pl-3" icon="money-bill-alt"></MDBIcon><label><NumberFormat value={this.state.costo} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /> CLP</label> </MDBCardBody>
                      </MDBCard>
                    </MDBCol>
                  </MDBRow>
                  <MDBRow>
                    <MDBCol>
                      <MDBCard >
                        <MDBCardHeader>Presupuesto Disponible</MDBCardHeader>
                        <MDBCardBody>
                          <MDBRow>
                            <MDBCol className="col-6">
                              <MDBCardTitle >Centro de costos:</MDBCardTitle>
                            </MDBCol>
                            <MDBCol className="col-6">
                              <MDBCardTitle style={{ color: 'blue' }}>{this.state.usuario.map(usu =>
                                usu.key == this.currUs ? this.state.CECO.map(ceco => ceco.key == usu.CECOus ?
                                  <h4>{ceco.nombre + ' ' + ceco.nro_ceco}</h4> : null) : null)}</MDBCardTitle>
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol>
                              <MDBCardTitle >Presupuesto gral. :</MDBCardTitle>
                            </MDBCol>
                            <MDBCol>
                              <MDBCardTitle style={{ color: 'green' }}>{this.state.usuario.map(usu =>
                                usu.key == this.currUs ? this.state.CECO.map(ceco => ceco.key == usu.CECOus ?
                                  <NumberFormat value={ceco.presupuesto} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /> : null) : null)}</MDBCardTitle>
                            </MDBCol>
                          </MDBRow>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBCol>
                  </MDBRow>
                  <MDBRow >
                    {/*Sección Botones Agregar Cotización*/}
                    <MDBCol className="text-center mt-2" >
                      <MDBBtn type="submit" size="md" color="green" size="md" onClick={this.toggle('Cot')}>Añadir Cotización</MDBBtn>
                      <MDBBtn size="md" color="primary" size="md" onClick={this.toggle('Cot')}>Volver</MDBBtn>
                    </MDBCol>
                  </MDBRow>
                </MDBModalBody>
              </form>
            </MDBModal>

          </MDBRow>


          <MDBRow >
            {/*Botones para volver */}
            <MDBCol className="text-center mb-3">
              <MDBBtn href="/dashboard" color="red" size="md">Volver</MDBBtn>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
        {/*Modal Aviso Solicitud Ingresada*/}
        <MDBModal size="sm" toggle={this.toggle('SolIngresada')} isOpen={this.state.modalSolIngresada} position="top" frame>
          <MDBModalBody className="text-center">
            <p style={{ color: '#007E33' }}><MDBIcon icon="calendar-plus" />  Solicitud Ingresada Satisfactoriamente</p>
          </MDBModalBody>
        </MDBModal>
        {/*Modal Aviso excede 3 cot */}
        <MDBModal isOpen={this.state.modalMax3Cot} toggle={this.toggle('Max3Cot')} size="sm" side position="top-right">
          <MDBModalHeader toggle={this.toggle('Max3Cot')}>Importante</MDBModalHeader>
          <MDBModalBody style={{ color: 'red' }}>
            <MDBIcon icon="times" /> No se pueden ingresar más de 3 cotizaciones
          </MDBModalBody>
        </MDBModal>
        {/*Modal Aviso Ingrese Cotización*/}
        <MDBModal isOpen={this.state.modalSolIngreseCot} toggle={this.toggle('SolIngreseCot')} size="sm" side position="top-right">
          <MDBModalHeader toggle={this.toggle('SolIngreseCot')}>Importante</MDBModalHeader>
          <MDBModalBody style={{ color: 'red' }}>
            <MDBIcon icon="times" /> Ingrese al menos una cotización
          </MDBModalBody>
        </MDBModal>
        {/*Modal Aviso Marque Cotización*/}
        <MDBModal isOpen={this.state.modalSolSeleccioneCot} toggle={this.toggle('SolSeleccioneCot')} size="sm" side position="top-right">
          <MDBModalHeader toggle={this.toggle('SolSeleccioneCot')}>Importante</MDBModalHeader>
          <MDBModalBody style={{ color: 'red' }}>
            <MDBIcon icon="times" /> Seleccione al menos una cotización
          </MDBModalBody>
        </MDBModal>
      </MDBCard>
    );
  }
}

export default FormNuevaSolSection;