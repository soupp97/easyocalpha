import React, { Component } from 'react';
import { MDBBtn, MDBFormInline, MDBTooltip, MDBModal, MDBModalBody, MDBModalHeader, MDBInput, MDBCard, MDBIcon, MDBRow, MDBContainer, MDBCol } from 'mdbreact';
import firebase from '../../../../Firebase';
import NumberFormat from 'react-number-format';
import { Table } from 'reactstrap';
import fileDownload from 'js-file-download';
import { Link } from 'react-router-dom';
class VerSolicitudPage extends Component {
    constructor(props) {
        super(props);
        this.storageRef = firebase.storage().ref();
        this.refBit = firebase.firestore().collection('bitacora');
        this.refBit2 = firebase.firestore().collection('bitacora').orderBy('fecha', 'desc');
        this.refUs = firebase.firestore().collection('usuario');
        this.refCot = firebase.firestore().collection('cotizacion');
        this.currUs = firebase.auth().currentUser.uid;
        this.unsuscribe = null;
        this.state = {
            solicitud: {},
            Cotis: [],//Cotizaciones
            cotPref: {},//Cotizacion Preferida
            bitacora: [],
            usuarios: [],//Usuarios
            nombres: '',
            comentario: '',
            apellidos: '',
            cargo: '',
            //Datos Bitacora
            estadoSol: '',
            fecha: '',
            estado: '',
            n_us: '',
            uid_sol: '',
            //Datos de Solicitud
            CECO: {},
            url: {},
            codSap: '',//Código Sap
            oc_pdf: '',
            urlPdf: '',
            selectedFile: {},//PDF OC
            cuenta: {},
            usuario: {},//Usuario que creo la solicitud
            keyCot: '',
            key2: '',
            key3: '',
            key4: '',
            key5: '',
            key6: '',//Key Usuario
            key: '',
        };
    }

    state = {
        modalSolGestionada: false,
        modalverBitacora: false,
        modalverCotizaciones: false,
        modalrechazarSolicitud: false,
        modalvalidarSolicitud: false,
        modalgenerarSolicitud: false,
        modalliberarSolicitud: false,
        modalcompletarSolicitud: false,
        modalconfirmarRecepcion: false
    }

    toggle = nr => () => {
        let modalName = 'modal' + nr;
        this.setState({
            [modalName]: !this.state[modalName]
        })
    }

    obtenerCotz() {
        const Cotis = [];
        for (let index = 0; index < this.state.solicitud.Cotz.length; index++) {
            const cot = this.state.solicitud.Cotz[index];
            firebase.firestore().collection('cotizacion').get().then((snapShots) => {
                snapShots.docs.map(
                    doc => doc.id == cot.cotKey ?
                        Cotis.push({
                            keyCot: doc.id,
                            data: doc.data()
                        })

                        : null
                )
            })
            let urlPdf = '';
            this.setState({ Cotis });

        }
        console.log(this.state);
    }

    obtenerCotPref() {
        const refCot = firebase.firestore().collection('cotizacion').doc(this.state.solicitud.cotPref);
        refCot.get().then((doc) => {
            if (doc.exists) {
                this.setState({
                    cotPref: doc.data(),
                    keyCot: doc.id,
                    isLoading: false
                });
                this.obtenerCotz();
            } else {
                console.log("No such document!");
            }
        })

    }

    submitRechazar = (e) => {
        e.preventDefault();
        const { comentario } = this.state;
        e.target.className += "was-validated";
        const refSol = firebase.firestore().collection('solicitud').doc(this.props.match.params.id);
        if (comentario.trim().length <= 0) {

        } else {

            refSol.update({
                estadoVal: 'Rechazada',
                estado: 'Rechazada'
            })

            let modalName = 'modalSolRechazada';
            this.setState({
                [modalName]: !this.state[modalName]
            })
            refSol.get().then((doc) => {
                if (doc.exists) {
                    this.setState({
                        solicitud: doc.data(),
                        key: doc.id,
                        isLoading: false
                    });
                    this.refBit.add({
                        uid_sol: this.props.match.params.id,
                        fecha: Date.now(),
                        n_us: this.currUs,
                        estadoSol: 'Solicitud ' + this.state.solicitud.estadoVal,
                        comentario
                    }).then((docRef) => {
                        this.setState({
                            uid_sol: '',
                            fecha: '',
                            n_us: '',
                            estadoSol: ''
                        })
                    });

                } else {
                    console.log("No such document!");
                }
            });

        }

    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value
        this.setState(state);
    }
    onChangePdf = (e) => {
        this.setState({
            selectedFile: e.target.files[0]
        })
    }


    submitSol = (e) => {
        e.preventDefault();
        const refSol = firebase.firestore().collection('solicitud').doc(this.props.match.params.id);
        const { comentario } = this.state;
        e.target.className += "was-validated";
        let st = '';
        let est = '';
        let modalName = '';
        if (comentario.trim().length <= 0) {

        } else {
            if (this.state.solicitud.estadoVal == "Por Validar") {
                st = "Validada";
                est = "En proceso";
                modalName = 'modalSolRechazada';
                
            } else if (this.state.solicitud.estadoVal == "Validada") {
                st = "Generada";
                est = "En proceso";
                modalName = "generarSolicitud";
                refSol.update({
                    codSap: this.state.codSap
                })
            } else if (this.state.solicitud.estadoVal == "Generada") {
                    st = "Liberada";
                    est = "En proceso";
                    modalName = "liberarSolicitud";                
            } else if (this.state.solicitud.estadoVal == "Liberada") {
                if (this.state.selectedFile.name == "") {
                } else {
                    st = "Completada";
                    est = "En proceso";
                    modalName = "completarSolicitud";
                    const pdRef = this.storageRef.child('OC/' + this.state.selectedFile.name);
                    pdRef.put(this.state.selectedFile).then(function (snapshot) {
                    });
                    refSol.update({
                        oc_pdf: this.state.selectedFile.name
                    })
                }
            } else if (this.state.solicitud.estadoVal == "Completada") {
                st = "Confirmada";
                est = "Completada";
                this.setState({
                    modalconfirmarRecepcion: !this.state.modalconfirmarRecepcion
                })
                const PR = this.state.CECO.presupuesto; //variable para rescatar el presupuesto actual
                //Una vez confirmada realiza el descuento
                const refCECO = firebase.firestore().collection('centro_costo').doc(this.state.cuenta.ceco_key);
                refCECO.update({ presupuesto: PR - this.state.solicitud.total_sol });
            }



            this.setState({
                [modalName]: !this.state[modalName]
            })

            //setea el estado
            refSol.update({
                estadoVal: [st],
                estado: est
            })
            //Muestra Modal
            let modalName = 'modalSolGestionada';
            this.setState({
                [modalName]: !this.state[modalName]
            })
            refSol.get().then((doc) => {
                if (doc.exists) {
                    this.setState({
                        solicitud: doc.data(),
                        key: doc.id,
                        isLoading: false
                    });
                    this.refBit.add({
                        uid_sol: this.props.match.params.id,
                        fecha: Date.now(),
                        n_us: this.currUs,
                        estadoSol: 'Solicitud ' + this.state.solicitud.estadoVal,
                        comentario
                    }).then((docRef) => {
                        this.setState({
                            uid_sol: '',
                            fecha: '',
                            n_us: '',
                            estadoSol: '',
                            comentario: ''
                        })
                    });

                } else {
                    console.log("No such document!");
                }
            });

        }
        //Agrega Bitacora

        //Refresca la solicitud

    }

    OnCollectionUs = (querySnapshot) => {
        const usuarios = [];
        querySnapshot.forEach((doc) => {
            const { nombres, cargo, apellidos } = doc.data();
            usuarios.push({
                key6: doc.id,
                doc, // DocumentSnapshot
                nombres,
                cargo,
                apellidos
            });
        });
        this.setState({
            usuarios
        });
        this.obtenerCotPref();
    }

    OnCollectionBit = (querySnapshot) => {
        const bitacora = [];
        querySnapshot.forEach((doc) => {
            const { estadoSol, fecha, n_us, comentario, uid_sol } = doc.data();
            bitacora.push({
                key5: doc.id,
                doc, // DocumentSnapshot
                estadoSol,
                fecha,
                n_us,
                comentario,
                uid_sol,
            });
        });
        this.setState({
            bitacora
        });
    }
    OnCollectionCta() {
        const refCta = firebase.firestore().collection('cuenta').doc(this.state.solicitud.nro_cta);
        refCta.get().then((doc) => {
            if (doc.exists) {
                this.setState({
                    cuenta: doc.data(),
                    key3: doc.id,
                    isLoading: false
                });
                this.OnCollectionCECO();
            } else {
                console.log("No such document!");
            }
        });
    }

    OnCollectionCECO() {
        const refCECO = firebase.firestore().collection('centro_costo').doc(this.state.cuenta.ceco_key);
        refCECO.get().then((doc) => {
            if (doc.exists) {
                this.setState({
                    CECO: doc.data(),
                    key4: doc.id,
                    isLoading: false
                });
                this.unsubscribe = this.refBit2.onSnapshot(this.OnCollectionBit);
                this.unsubscribe = this.refUs.onSnapshot(this.OnCollectionUs);

            } else {
                console.log("No such document!");
            }
        });
    }

    componentDidMount() {
        //Datos de la solicitud

        const refSol = firebase.firestore().collection('solicitud').doc(this.props.match.params.id);
        refSol.get().then((doc) => {
            if (doc.exists) {
                this.setState({
                    solicitud: doc.data(),
                    key: doc.id,
                    isLoading: false
                });
                this.setState({ url: this.storageRef.child('OC/' + this.state.solicitud.oc_pdf).getDownloadURL() })


                //Datos del usuario
                const refUs = firebase.firestore().collection('usuario').doc(this.state.solicitud.usuKey);
                refUs.get().then((doc) => {
                    if (doc.exists) {
                        this.setState({
                            usuario: doc.data(),
                            key2: doc.id,
                            isLoading: false
                        });

                    } else {
                        console.log("No such document!");
                    }
                });
                this.OnCollectionCta();

            } else {
                console.log("No such document!");
            }
        });





    }
    render() {
        const { comentario, codSap, oc_pdf } = this.state;
        return (
            <MDBContainer fluid>
                <MDBRow>
                    <MDBCol>
                        <MDBCard className="cascading-admin-card ">
                            <div className="admin-up px-2" >
                                <MDBBtn color="elegant" disabled className="p-2"><strong>Solicitud de {this.state.solicitud.tipo_sol}</strong></MDBBtn>
                                <h6 style={{ fontSize: '12px' }}><strong>Fecha Emisión: </strong>{Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(this.state.solicitud.fecha_emision)}</h6>
                            </div>
                            <MDBRow middle="true">
                                <MDBCol middle="true">
                                    <MDBBtn color="primary" onClick={this.toggle('verCotizaciones')} className="p-2 m-2">Cotizaciones</MDBBtn>
                                </MDBCol>
                            </MDBRow>
                        </MDBCard>
                        <MDBCard className="cascading-admin-card mt-3">
                            <div className="admin-up px-2 mb-2" >
                                <MDBBtn color="elegant" disabled className="p-2"><strong>Solicitante</strong></MDBBtn>
                                <h6 className="pt-2" style={{ fontSize: '12px' }}><strong>RUT: </strong>{this.state.usuario.rut}</h6>
                                <h6 style={{ fontSize: '12px' }}><strong>Nombre: </strong>{this.state.usuario.nombres + ' ' + this.state.usuario.apellidos}</h6>
                                <h6 style={{ fontSize: '12px' }}><strong>Área: </strong>{this.state.solicitud.area_sol}</h6>
                                <h6 style={{ fontSize: '12px' }}><strong>Cargo: </strong>{this.state.usuario.cargo}</h6>
                            </div>
                        </MDBCard>
                        <MDBCard className="cascading-admin-card mt-3">
                            <div className="admin-up px-2 mb-2" >
                                <MDBBtn color="elegant" disabled className="p-2"><strong>Imputación</strong></MDBBtn>
                                <h6 style={{ fontSize: '12px' }}><strong>Centro de costos (CECO): </strong>{this.state.CECO.nombre + ' ' + this.state.CECO.nro_ceco}</h6>
                                <h6 style={{ fontSize: '12px' }}><strong>Cuenta: </strong>{this.state.cuenta.nombre_cta + ' ' + this.state.cuenta.nro_cuenta} </h6>
                                <h6 style={{ fontSize: '12px' }}><strong>OCO: </strong>{this.state.solicitud.oco}</h6>
                            </div>
                        </MDBCard>
                    </MDBCol >
                    <MDBCol size="4">
                        <MDBCard className="cascading-admin-card">
                            <div className="admin-up px-2 mb-2" >
                                <MDBBtn color="elegant" disabled className="p-2"><strong>Justificación</strong></MDBBtn>
                                <h6 className="pt-2" style={{ fontSize: '15px' }}><strong>{this.state.solicitud.desc_sol}</strong></h6>
                            </div>
                        </MDBCard>
                        <MDBCard className="cascading-admin-card mt-3">
                            <div className="admin-up px-2 mb-2" >
                                <MDBBtn color="elegant" disabled className="p-2"><strong>{this.state.solicitud.tipo_sol}</strong></MDBBtn>
                                <h6 className="pt-2" style={{ fontSize: '12px' }}><strong>Código SAP: </strong>{this.state.solicitud.codSap}</h6>
                                <h6 style={{ fontSize: '12px' }}><strong>Estado: </strong><MDBBtn color={
                                    this.state.solicitud.estadoVal == "Por Validar" ? "primary" :
                                        this.state.solicitud.estadoVal == "Generada" ? "default" :
                                            this.state.solicitud.estadoVal == "Rechazada" ? "danger" :
                                                this.state.solicitud.estadoVal == "Validada" ? "secondary" :
                                                    this.state.solicitud.estadoVal == "Liberada" ? "light-green" :
                                                        this.state.solicitud.estadoVal == "Completada" ? "light-green" :
                                                            this.state.solicitud.estadoVal == "Confirmada" ? "dark-green" : "warning"} size="sm" className="px-2 m-1" disabled outline><strong>{this.state.solicitud.estadoVal}</strong></MDBBtn></h6>
                                <h6 style={{ fontSize: '12px' }}><strong>Servicio: </strong>{this.state.cotPref.nm_servicio}</h6>
                                <h6 style={{ fontSize: '12px' }}><strong>Monto Estimado: </strong><NumberFormat value={this.state.solicitud.total_sol} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h6>
                                {this.state.solicitud.estadoVal == "Por Validar" ? null :
                                    this.state.solicitud.estadoVal == "Validada" ? null :
                                        this.state.solicitud.estadoVal == "Generada" ? null :
                                            this.state.solicitud.estadoVal == "Rechazada" ? null :
                                                <h6 style={{ fontSize: '12px' }}><strong>Orden de Compra SAP: </strong><a><u style={{ color: 'blue' }}>{this.state.solicitud.oc_pdf}</u></a><MDBBtn target="_blank" href={this.state.url.i} color="primary" className="m-1 p-1 ml-3">Descargar</MDBBtn></h6>}
                            </div>
                        </MDBCard>
                        {this.state.solicitud.estadoVal == "Rechazada" ? null :
                            this.state.solicitud.estadoVal == "Confirmada" ? null :
                                this.props.data.currentUs.cargo == "Jefe Administrativo" ?
                                    this.state.solicitud.estadoVal == "Por Validar" ?
                                        <MDBCard className="mt-3">
                                            <MDBContainer fluid>
                                                <MDBRow className="mx-0 mt-2 mb-1">
                                                    <MDBBtn className="p-2" onClick={this.toggle("validarSolicitud")} block color="primary"> <a>Validar Solicitud</a></MDBBtn>


                                                </MDBRow>
                                                <MDBRow className="mx-0 mb-2 mt-1">

                                                    <MDBBtn className="p-2" onClick={this.toggle("rechazarSolicitud")} block color="danger">Rechazar Solicitud</MDBBtn>


                                                </MDBRow>
                                            </MDBContainer>
                                        </MDBCard> : null
                                    : this.props.data.currentUs.cargo == "Operador" ?
                                        this.state.solicitud.estadoVal == "Validada" ?
                                            <MDBCard className="mt-3">
                                                <MDBContainer fluid>
                                                    <MDBRow className="mx-0 mt-2 mb-1">
                                                        <MDBBtn className="p-2" onClick={this.toggle("generarSolicitud")} block color="primary"><a>Generar Solicitud</a></MDBBtn>
                                                    </MDBRow>
                                                    <MDBRow className="mx-0 mb-2 mt-1">

                                                        <MDBBtn className="p-2" onClick={this.toggle("rechazarSolicitud")} block color="danger">Rechazar Solicitud</MDBBtn>


                                                    </MDBRow>
                                                </MDBContainer>
                                            </MDBCard> :
                                            this.state.solicitud.estadoVal == "Liberada" ?
                                                <MDBCard className="mt-3">
                                                    <MDBContainer fluid>
                                                        <MDBRow className="mx-0 mt-2 mb-1">
                                                            <MDBBtn className="p-2" onClick={this.toggle("completarSolicitud")} block color="primary"><a>Completar Solicitud</a></MDBBtn>
                                                        </MDBRow>
                                                        <MDBRow className="mx-0 mb-2 mt-1">

                                                            <MDBBtn className="p-2" onClick={this.toggle("rechazarSolicitud")} block color="danger">Rechazar Solicitud</MDBBtn>


                                                        </MDBRow>
                                                    </MDBContainer>
                                                </MDBCard> : null
                                        : this.props.data.currentUs.cargo == "Administrador" ?
                                            this.state.solicitud.estadoVal == "Generada" ?
                                                <MDBCard className="mt-3">
                                                    <MDBContainer fluid>
                                                        <MDBRow className="mx-0 mt-2 mb-1">
                                                            <MDBBtn className="p-2" onClick={this.toggle("liberarSolicitud")} block color="primary"><a>Liberar Solicitud</a></MDBBtn>
                                                        </MDBRow>
                                                        <MDBRow className="mx-0 mb-2 mt-1">

                                                            <MDBBtn className="p-2" onClick={this.toggle("rechazarSolicitud")} block color="danger">Rechazar Solicitud</MDBBtn>


                                                        </MDBRow>
                                                    </MDBContainer>
                                                </MDBCard> : null
                                            : this.props.data.currentUs.cargo == "Solicitante" ?
                                                this.state.solicitud.estadoVal == "Completada" ?
                                                    <MDBCard className="mt-3">
                                                        <MDBContainer fluid>
                                                            <MDBRow className="mx-0 mt-2 mb-1">
                                                                <MDBBtn className="p-2" onClick={this.toggle("confirmarRecepcion")} block color="primary"><a>Confirmar Recepción</a></MDBBtn>
                                                            </MDBRow>
                                                            <MDBRow className="mx-0 mb-2 mt-1">

                                                                <MDBBtn className="p-2" onClick={this.toggle("rechazarSolicitud")} block color="danger">Rechazar Recepción</MDBBtn>


                                                            </MDBRow>
                                                        </MDBContainer>
                                                    </MDBCard> : null : null}


                    </MDBCol>
                    <MDBModal toggle={
                        this.state.solicitud.estadoVal == 'Por Validar' ? this.toggle("validarSolicitud") :
                            this.state.solicitud.estadoVal == 'Validada' ? this.toggle("generarSolicitud") :
                                this.state.solicitud.estadoVal == 'Generada' ? this.toggle("liberarSolicitud") :
                                    this.state.solicitud.estadoVal == 'Liberada' ? this.toggle("completarSolicitud") : this.toggle("confirmarRecepcion")}
                        isOpen={
                            this.state.solicitud.estadoVal == 'Por Validar' ? this.state.modalvalidarSolicitud :
                                this.state.solicitud.estadoVal == 'Validada' ? this.state.modalgenerarSolicitud :
                                    this.state.solicitud.estadoVal == 'Generada' ? this.state.modalliberarSolicitud :
                                        this.state.solicitud.estadoVal == 'Liberada' ? this.state.modalcompletarSolicitud : this.state.modalconfirmarRecepcion
                        } size="md" >
                        <MDBModalHeader>Ingrese Un comentario sobre esta acción</MDBModalHeader>
                        <MDBModalBody >
                            <form noValidate onSubmit={this.submitSol}>
                                <MDBRow middle="true">
                                    <MDBCol>
                                        <MDBInput required name="comentario" className="form-control" value={comentario} onChange={this.onChange} type="textarea" label="Justificación" rows="5" icon="pencil-alt"></MDBInput>
                                    </MDBCol>
                                </MDBRow>
                                {this.state.solicitud.estadoVal == 'Validada' ?
                                    <MDBRow middle="true">
                                        <MDBCol>
                                            <MDBInput required name="codSap" className="form-control" value={codSap} onChange={this.onChange} type="number" label="OC SAP" rows="1" icon="book"></MDBInput>
                                        </MDBCol>
                                    </MDBRow> : this.state.solicitud.estadoVal == 'Liberada' ?
                                        <MDBRow middle="true">
                                            <MDBCol>
                                                <div className="input-group">
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text" id="oc_pdf">Subir PDF</span>
                                                    </div>
                                                    <div className="custom-file">
                                                        <input required onChange={this.onChangePdf} type="file" className="custom-file-input" name="oc_pdf"
                                                            aria-describedby="oc_pdf" />
                                                        <label className="custom-file-label" htmlFor="oc_pdf">{this.state.selectedFile.name}</label>
                                                    </div>
                                                </div>
                                            </MDBCol>
                                        </MDBRow> : null}
                                {/*Botones Agregar*/}
                                <MDBRow>
                                    <MDBCol className="text-center">

                                        <MDBBtn type="submit" color="green" size="md" >{
                                                this.state.solicitud.estadoVal == 'Por Validar' ? <a>Validar Solicitud</a> :
                                                    this.state.solicitud.estadoVal == 'Validada' ? <a>Generar Solicitud</a> :
                                                        this.state.solicitud.estadoVal == 'Generada' ? <a>Liberar Solicitud</a> :
                                                            this.state.solicitud.estadoVal == 'Liberada' ? <a>Completar Solicitud</a> : <a>Confirmar Recepción</a>
                                            }</MDBBtn>
                                        <MDBBtn color="primary" size="md" onClick={
                                            this.state.solicitud.estadoVal == 'Por Validar' ? this.toggle("validarSolicitud") :
                                                this.state.solicitud.estadoVal == 'Validada' ? this.toggle("generarSolicitud") :
                                                    this.state.solicitud.estadoVal == 'Generada' ? this.toggle("liberarSolicitud") :
                                                        this.state.solicitud.estadoVal == 'Liberada' ? this.toggle("completarSolicitud") : this.toggle("confirmarRecepcion")}>Volver</MDBBtn>
                                    </MDBCol>
                                </MDBRow>
                            </form>
                        </MDBModalBody>
                    </MDBModal>
                    <MDBModal toggle={this.toggle("rechazarSolicitud")}
                        isOpen={this.state.modalrechazarSolicitud} size="md" backdrop={false}>
                        <MDBModalHeader>Está Rechazando una Solicitud</MDBModalHeader>
                        <MDBModalBody >
                            <form noValidate onSubmit={this.submitRechazar}>
                                <MDBRow middle="true">
                                    <MDBCol>
                                        <h6>¿Está seguro/a que desea rechazar esta solicitud?</h6>
                                        <h6>Indique sus razones aquí abajo.</h6>
                                        <MDBInput required name="comentario" className="form-control" value={comentario} onChange={this.onChange} type="textarea" label="Justificación" rows="5" icon="pencil-alt" />
                                    </MDBCol>
                                </MDBRow>
                                {/*Botones Agregar*/}
                                <MDBRow>
                                    <MDBCol className="text-center">

                                        <MDBBtn type="submit" color="green" size="md" onClick={this.toggle("rechazarSolicitud")}>Rechazar Solicitud</MDBBtn>
                                        <MDBBtn color="primary" size="md" onClick={this.toggle("rechazarSolicitud")}>Volver</MDBBtn>
                                    </MDBCol>
                                </MDBRow>
                            </form>
                        </MDBModalBody>
                    </MDBModal>
                    <MDBCol>
                        {/*Seccion Bitacora */}
                        <MDBCard>
                            <MDBRow middle="true" className="mt-2">
                                <MDBCol>
                                    <h3>Bitácora</h3>
                                </MDBCol>
                            </MDBRow>
                            <MDBRow>
                                <MDBCol>
                                    {this.state.bitacora.map(bit => bit.uid_sol == this.props.match.params.id ?
                                        <MDBCard className="m-3">
                                            <MDBRow >
                                                <MDBCol className="col-2 " >
                                                    <MDBTooltip placement="left">
                                                        <MDBBtn style={{ borderRadius: '50%' }} color="info" className="p-1 "><MDBIcon size="2x" icon="wrench" className="m-1" /></MDBBtn>
                                                        <MDBContainer >
                                                            {bit.comentario}
                                                        </MDBContainer>
                                                    </MDBTooltip>
                                                </MDBCol>
                                                <MDBCol middle="true" className="col-10">
                                                    <MDBRow className="m-1 mx-1 px-1" middle="true">
                                                        <MDBCol className="col-5" middle="true">
                                                            <MDBFormInline className="md-form m-0">
                                                                {this.state.usuarios.map(us => us.key6 == bit.n_us ? <h6 className="mx-2 pt-1" style={{ fontSize: '14px' }}>{us.nombres + ' ' + us.apellidos}</h6> : null)}
                                                            </MDBFormInline>
                                                        </MDBCol>
                                                        <MDBCol className="col-2">
                                                            <MDBIcon className="pt-3 mx-2" icon="grip-lines-vertical" />
                                                        </MDBCol>
                                                        <MDBCol className="col-5">
                                                            <h6 className="mx-2 pt-1" style={{ fontSize: '12px' }}>{Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(bit.fecha)}</h6>

                                                        </MDBCol>
                                                    </MDBRow>
                                                    <MDBRow className="m-1" middle="true">
                                                        <MDBCol middle="true">
                                                            <h6 style={{ textAlign: 'center' }}><strong>{bit.estadoSol}</strong></h6>
                                                        </MDBCol>
                                                    </MDBRow>
                                                </MDBCol>
                                            </MDBRow>
                                        </MDBCard> : null

                                    )}

                                </MDBCol>
                            </MDBRow>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
                <MDBModal size="sm" toggle={this.toggle('SolGestionada')} isOpen={this.state.modalSolGestionada} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#007E33' }}><MDBIcon icon="tools" />  Solicitud Gestionada Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                <MDBModal size="lg" toggle={this.toggle('verCotizaciones')} isOpen={this.state.modalverCotizaciones} position="top" >
                    <MDBModalHeader>
                        Cotizaciones
                    </MDBModalHeader>
                    <MDBModalBody className="text-center">

                        <Table bordered responsive striped>
                            <thead>
                                <tr>
                                    <th>Razón Social</th>
                                    <th>Servicio</th>
                                    <th>Costo</th>
                                    <th>Preferida.</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.Cotis.map(cot =>
                                    <tr>
                                        <td>{cot.data.nm_proveedor}</td>
                                        <td>{cot.data.nm_servicio}</td>
                                        <td><NumberFormat value={cot.data.costo} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></td>
                                        <td>{cot.keyCot == this.state.solicitud.cotPref ? <MDBIcon size="lg" icon="check-circle"></MDBIcon> : null}</td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>

                    </MDBModalBody>
                </MDBModal>
            </MDBContainer>
        )

    }
}

export default VerSolicitudPage;