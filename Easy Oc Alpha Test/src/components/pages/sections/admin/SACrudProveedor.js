import React, { Component } from 'react';
import { MDBDataTable, MDBCard, MDBCardHeader, MDBCardBody, MDBIcon, MDBBtn, MDBModal, MDBCol, MDBContainer, MDBInput, MDBModalBody, MDBRow, MDBModalHeader } from "mdbreact";
import { Table, Container } from 'reactstrap';
import firebase from '../../../../Firebase';
import { userInfo } from 'os';
import FirebaseUsers from '../../../../FirebaseUsers';
import { validate, clean, format } from 'rut.js';



class SACrudProveedor extends Component {
    constructor(props) {
        super(props);
        this.ref = firebase.firestore().collection('proveedor');
        this.state = {
            proveedor: [],
            rut_proveedor: '',
            jefe: '',
            razon_social: '',
            contraseña: '',
            correo_contacto: '',
            rubro: '',
            fono: '',
            casa_central: '',
            web: '',
            key: ''
        };
    }

    state = {
        rut_proveedorval: '',
        usr: '',
        modal1: false,
        modal2: false,
        modal3: false,
        modal4: false,
        modal5: false,
        modal6: false
    }

    agregar = (tg) => () => {
        this.toggle(tg);
    }

    confirmarEditar = (tg1, tg2, tg3) => () => {
        let modalName1 = 'modal' + tg1;
        let modalName2 = 'modal' + tg2;
        let modalName3 = 'modal' + tg3;
        this.setState({
            [modalName1]: !this.state[modalName1],
            [modalName2]: !this.state[modalName2],
            [modalName3]: !this.state[modalName3],
        })
    }

    eliminar = (usr1, tg) => () => {
        console.log(tg, usr1);
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            usr: usr1
        })


    }
    editar = (usr1, tg) => () => {
        const ref2 = firebase.firestore().collection('proveedor').doc(usr1);
        ref2.get().then((doc) => {
            if (doc.exists) {
                const user1 = doc.data();
                this.setState({
                    key: doc.id,
                    rut_proveedor: user1.rut_proveedor,
                    jefe: user1.jefe,
                    razon_social: user1.razon_social,
                    correo_contacto: user1.correo_contacto,
                    rubro: user1.rubro,
                    fono: user1.fono,
                    contraseña: user1.contraseña,
                    casa_central: user1.casa_central,
                    web: user1.web
                });
            } else {
                console.log("No such document!");
            }
        });
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
        })


    }

    toggle = nr => () => {
        console.log(nr);
        let modalName = 'modal' + nr;
        this.setState({
            [modalName]: !this.state[modalName]
        })
    }

    onChange = (e) => {
        const state = this.state
        if (e.target.name == 'rut_proveedor') {
            e.target.value = clean(e.target.value);
            e.target.value = format(e.target.value);
            state[e.target.name] = e.target.value
            this.setState(state);
            validate(e.target.value) ?
                this.setState({ rut_proveedorval: <p style={{ color: 'green' }}> Rut valido <MDBIcon icon="check-circle"></MDBIcon></p> }) :
                this.setState({ rut_proveedorval: <p style={{ color: 'red' }}> Rut no valido <MDBIcon icon="times-circle"></MDBIcon></p> });
            console.log(e.target.value);
        } else {
            state[e.target.name] = e.target.value
            this.setState(state);
            console.log(e.target.name);
        }

    }
    //Función para Editar en Firebase
    onSubmit2 = (e) => {
        e.preventDefault();
        e.target.className += "was-validated";

        const { rut_proveedor, jefe, razon_social,correo_contacto, rubro, fono, casa_central, web } = this.state;
        const updateRef = firebase.firestore().collection('proveedor').doc(this.state.key);
        updateRef.set({
            rut_proveedor,
            jefe,
            razon_social,
            correo_contacto,
            rubro,
            fono,
            casa_central,
            web
        }).then((docRef) => {
            this.setState({
                key: '',
                rut_proveedor: '',
                jefe: '',
                razon_social: '',
                correo_contacto: '',
                rubro: '',
                fono: '',
                casa_central: '',
                web: ''
            });
            this.props.history.push("#")
        })
            .catch((error) => {
                console.error("Error adding document: ", error);
            });

    }
    //Función para Agregar en Firebase
    onSubmit = (e) => {
        e.preventDefault();
        e.target.className += "was-validated";

        const { rut_proveedor, jefe, razon_social, contraseña, correo_contacto, rubro, fono, casa_central, web } = this.state;

        if (!validate(rut_proveedor)) {
        } else if (jefe.trim().length <= 0) {
        } else if (razon_social.trim().length <= 0) {
        } else if (correo_contacto.trim().length <= 0) {
        } else if (contraseña.trim().length <= 0) {
        } else if (rubro.trim().length <= 0) {
        } else if (fono.trim().length <= 0) {
        } else if (casa_central.trim().length <= 0) {
        } else {
        FirebaseUsers.auth().createUserWithEmailAndPassword(this.state.correo_contacto, this.state.contraseña).then((cred) => {
            cred.user.updateProfile({
                displayName: razon_social
            }).then();
            this.ref.doc(cred.user.uid).set({
                rut_proveedor,
                jefe,
                razon_social,
                correo_contacto,
                rubro,
                fono,
                contraseña,
                casa_central,
                web
            }).then((docRef) => {
                this.setState({
                    rut_proveedor: '',
                    jefe: '',
                    razon_social: '',
                    correo_contacto: '',
                    contraseña: '',
                    rubro: '',
                    fono: '',
                    casa_central: '',
                    web: ''
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });

        }).catch((error) => {
            var code = error.code;
            var message = error.message;
            if (code == null) {
                this.setState({ estado: "" });
            } else {
                this.setState({ estado: "El Email o la contraseña son incorrectos." });
            }
            console.log(code);
        });
        FirebaseUsers.auth().signOut();
    }



    }

    onCollectionUpdate = (querySnapshot) => {
        const proveedor = [];
        querySnapshot.forEach((doc) => {
            const { rut_proveedor, jefe, contraseña, razon_social, correo_contacto, rubro, fono, casa_central, web } = doc.data();
            proveedor.push({
                key: doc.id,
                doc, // DocumentSnapshot
                rut_proveedor,
                jefe,
                razon_social,
                correo_contacto,
                rubro,
                fono,
                contraseña,
                casa_central,
                web
            });
        });
        this.setState({
            proveedor
        });
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }

    delete(id, tg, tg2) {
        firebase.firestore().collection('proveedor').doc(id).delete().then(() => {
            console.log("Document successfully deleted!");
            this.props.history.push("#")

        }).catch((error) => {
            console.error("Error removing document: ", error);
        });
        let modalName = 'modal' + tg;
        let modalName2 = 'modal' + tg2;
        this.setState({
            [modalName]: !this.state[modalName],
            [modalName2]: !this.state[modalName2]
        })
    }

    render() {
        const { rut_proveedor, jefe, razon_social, correo_contacto, rubro, fono, contraseña, casa_central, web, usr } = this.state;
        return (
            <Container>
                <MDBCard>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <MDBCardHeader><h3>Gestión de Proveedores</h3></MDBCardHeader>
                        </div>
                        {/*tabla de proveedores */}
                        <Table bordered responsive striped>
                            <thead>
                                <tr>
                                    <th style={{ width: '30%' }}>Rut proveedor</th>
                                    <th>Jefe</th>
                                    <th style={{ width: '40%' }}>Razon social</th>
                                    <th>Correo contacto</th>
                                    <th>Rubro</th>
                                    <th>Fono</th>
                                    <th>Casa central</th>
                                    <th>Web</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.proveedor.map(proveedor =>
                                    <tr>
                                        <td>{proveedor.rut_proveedor}</td>
                                        <td>{proveedor.jefe}</td>
                                        <td>{proveedor.razon_social}</td>
                                        <td>{proveedor.correo_contacto}</td>
                                        <td>{proveedor.rubro}</td>
                                        <td>{proveedor.fono}</td>
                                        <td>{proveedor.casa_central}</td>
                                        <td>{proveedor.web}</td>
                                        <td scope="row">
                                            <MDBBtn color="yellow" onClick={this.editar(proveedor.key, 2)} ><MDBIcon size="lg" icon="edit" /></MDBBtn>
                                            <MDBBtn color="red" onClick={this.eliminar(proveedor.key, 5)} ><MDBIcon size="lg" icon="times-circle" /></MDBBtn>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        {/*-Sección Modal Eliminar */}
                        <MDBModal className="pl-5" size="sm" toggle={this.toggle(5)} isOpen={this.state.modal5} backdrop={true}>
                            <MDBModalHeader>¿Desea eliminarlo?</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer center>
                                    Esta acción es irreversible
                                <br></br>
                                    <br></br>
                                    <MDBCol center>
                                        <MDBBtn color="red" size="md" onClick={this.delete.bind(this, usr, 5, 6)} >Si</MDBBtn>
                                        <MDBBtn color="blue" size="md" onClick={this.toggle(5)}>No</MDBBtn>
                                    </MDBCol>
                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>

                        {/*Botón para Agregar */}
                        <MDBBtn size="md" md="4" block color="green" onClick={this.toggle(1)}>Agregar proveedor</MDBBtn>
                        {/*-Sección Modal Agregar */}
                        <MDBModal  isOpen={this.state.modal1} size="md" >
                            <MDBModalHeader>Ingrese los datos del proveedor</MDBModalHeader>
                            <MDBModalBody >

                                <form noValidate onSubmit={this.onSubmit}>
                                    <MDBRow>
                                        <MDBCol>
                                            <MDBRow>
                                                <MDBCol>
                                                    <MDBInput label="Rut Proveedor" outline required className="form-control" name="rut_proveedor" value={rut_proveedor} onChange={this.onChange} ></MDBInput>
                                                    <MDBInput label="Jefe" outline required className="form-control" name="jefe" onChange={this.onChange} value={jefe} ></MDBInput>
                                                    <MDBInput label="Razón Social" outline required className="form-control" name="razon_social" onChange={this.onChange} value={razon_social} ></MDBInput>
                                                    <MDBInput label="Correo de Contacto:" outline required className="form-control" name="correo_contacto" type="email" onChange={this.onChange} value={correo_contacto} ></MDBInput>
                                                    <MDBInput label="Contraseña" outline required className="form-control" name="contraseña" type="password" onChange={this.onChange} value={contraseña} ></MDBInput>
                                                </MDBCol>
                                                <MDBCol>
                                                    <MDBInput label="Rubro" outline required className="form-control" name="rubro" onChange={this.onChange} value={rubro} ></MDBInput>
                                                    <MDBInput label="Fono" outline required className="form-control" name="fono" onChange={this.onChange} value={fono} ></MDBInput>
                                                    <MDBInput label="Dirección Casa Central" outline required className="form-control" name="casa_central" onChange={this.onChange} value={casa_central} ></MDBInput>
                                                    <MDBInput label="Página Web" outline className="form-control" name="web" onChange={this.onChange} value={web} ></MDBInput>
                                                </MDBCol>
                                            </MDBRow>



                                        </MDBCol>

                                    </MDBRow>
                                    {/*Botones Agregar proveedor */}
                                    <MDBRow>
                                        <MDBCol className="text-center">
                                            <MDBBtn type="submit" color="green" size="md" onClick={this.agregar(1)}>Agregar</MDBBtn>
                                            <MDBBtn color="primary" size="md" onClick={this.toggle(1)}>Volver</MDBBtn>
                                        </MDBCol>
                                        <MDBCol className="pt-2">
                                                {this.state.rut_proveedorval}
                                        </MDBCol>
                                    </MDBRow>


                                </form>




                            </MDBModalBody>
                        </MDBModal>
                        {/*Sección Modal Editar */}
                        <MDBModal toggle={this.toggle(2)} isOpen={this.state.modal2} size="md" backdrop={false}>
                            <MDBModalHeader>Ingrese los datos del Proveedor</MDBModalHeader>
                            <MDBModalBody >
                                <MDBCol >
                                    <form noValidate onSubmit={this.onSubmit2}>
                                        <MDBRow>
                                            <MDBCol>
                                                <MDBRow>
                                                    <MDBCol>
                                                        <MDBInput label="Rut Proveedor" outline required className="form-control" name="rut_proveedor" value={rut_proveedor} onChange={this.onChange} ></MDBInput>
                                                        <MDBInput label="Jefe" outline required className="form-control" name="jefe" onChange={this.onChange} value={jefe} ></MDBInput>
                                                        <MDBInput label="Razón Social" outline required className="form-control" name="razon_social" onChange={this.onChange} value={razon_social} ></MDBInput>
                                                        <MDBInput label="Correo de Contacto:" outline required className="form-control" name="correo_contacto" type="email" onChange={this.onChange} value={correo_contacto} ></MDBInput>
                                                    </MDBCol>
                                                    <MDBCol>
                                                        <MDBInput label="Rubro" outline required className="form-control" name="rubro" onChange={this.onChange} value={rubro} ></MDBInput>
                                                        <MDBInput label="Fono" outline required className="form-control" name="fono" onChange={this.onChange} value={fono} ></MDBInput>
                                                        <MDBInput label="Dirección Casa Central" outline required className="form-control" name="casa_central" onChange={this.onChange} value={casa_central} ></MDBInput>
                                                        <MDBInput label="Página Web" outline required className="form-control" name="web" onChange={this.onChange} value={web} ></MDBInput>
                                                    </MDBCol>
                                                </MDBRow>

                                            </MDBCol>

                                        </MDBRow>
                                        {/*Botones Agregar Proveedor */}
                                        <MDBRow>
                                            <MDBCol className="text-center">
                                                <MDBBtn color="warning" size="md" onClick={this.toggle(3)}>Editar</MDBBtn>
                                                <MDBBtn color="primary" size="md" onClick={this.toggle(2)}>Volver</MDBBtn>
                                            </MDBCol>
                                        </MDBRow>

                                        {/*Sección Modal Confirmar Editar */}
                                        <MDBModal className="pl-5" size="sm" toggle={this.toggle(3)} isOpen={this.state.modal3} backdrop={true}>
                                            <MDBModalHeader>¿Está seguro?</MDBModalHeader>
                                            <MDBModalBody>
                                                <MDBContainer center>
                                                    Está seguro que desea editar este Proveedor?
                                                    <br></br>
                                                    <br></br>
                                                    <MDBCol >
                                                        <MDBBtn type="submit" color="warning" size="md" onClick={this.confirmarEditar(2, 3, 4)} >Si</MDBBtn>
                                                        <MDBBtn color="primary" size="md" onClick={this.toggle(3)}>No</MDBBtn>
                                                    </MDBCol>
                                                </MDBContainer>
                                            </MDBModalBody>
                                        </MDBModal>

                                    </form>
                                </MDBCol>
                            </MDBModalBody>
                        </MDBModal>
                    </div>
                </MDBCard>
                <MDBModal size="sm" toggle={this.toggle(4)} isOpen={this.state.modal4} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#FBB138' }}><MDBIcon icon="exclamation-triangle" />   Proveedor Editado Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                <MDBModal toggle={this.toggle(6)} isOpen={this.state.modal6} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#C51E1E' }}><MDBIcon icon="user-times" />   Proveedor Eliminado Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
            </Container>
        );
    }
}

export default SACrudProveedor;