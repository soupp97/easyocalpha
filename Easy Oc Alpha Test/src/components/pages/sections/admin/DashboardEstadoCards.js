import React, { Component } from "react";
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBTable, MDBTableHead, MDBTableBody, MDBCardBody, MDBCard, MDBIcon, MDBRow, MDBContainer, MDBCol, MDBCardText } from 'mdbreact';
import firebase from '../../../../Firebase';
class DashboardEstadoCards extends Component {
    constructor(props) {
        super(props);
        this.refSol = firebase.firestore().collection('solicitud').where('usuKey', '==', this.props.KeyUs);
        this.refSolTodas = firebase.firestore().collection('solicitud');
        this.unsuscribe = null;
        this.state = {
            solicitud: [],//Contadores
            totalCountNu: 0,
            totalCountPr: 0,
            totalCountR: 0,
            totalCountVen: 0
        }
    }

    contadores() {
        let totalCountNu = 0;
        let totalCountPr = 0;
        let totalCountR = 0;
        let totalCountVen = 0;
        for (let index = 0; index < this.state.solicitud.length; index++) {
            const sol = this.state.solicitud[index];
            if (sol.estado == "Vencida") {
                totalCountVen += 1;
            } else if (sol.estado == "Nueva") {
                totalCountNu += 1;
            } else if (sol.estado == "Rechazada"){
                totalCountR += 1;
            } else if (sol.estado == "Completada"){
            }else {
                totalCountPr += 1;
            }
        }
        this.setState({ totalCountNu,totalCountPr,totalCountR,totalCountVen });
    }

    componentDidMount() {
        if(this.props.Us.cargo == "Solicitante"){
            this.unsubscribe = this.refSol.onSnapshot(this.onCollectionUpdateSol);
        }else {
            this.unsubscribe = this.refSolTodas.onSnapshot(this.onCollectionUpdateSol);
        }
        
        
    }

    onCollectionUpdateSol = (querySnapshot) => {
        const solicitud = [];
        querySnapshot.forEach((doc) => {
            const { estado } = doc.data();
            solicitud.push({
                key: doc.id,
                doc, // DocumentSnapshot
                estado

            });

        });
        this.setState({
            solicitud
        });
        this.contadores();
    }

    render() {
        return (
            <div>
                <MDBRow>
                    <MDBCol md="6" className="div2">
                        {/* --Card Vencidas-- */}
                        <MDBCard className="cascading-admin-card" >
                            <div className="admin-up">
                                <MDBIcon icon="exclamation-triangle" className="warning-color-dark" />
                                <div className="data">
                                    <h3>VENCIDAS</h3>
                                    <h4>
                                        <strong>{this.state.totalCountVen}</strong>

                                    </h4>
                                </div>
                            </div>

                        </MDBCard>
                    </MDBCol>
                    <MDBCol md="6" className="div2">
                        {/* --Card Nuevas-- */}
                        <MDBCard className="cascading-admin-card">
                            <div className="admin-up">
                                <MDBIcon icon="lightbulb" className="info-color-dark" />
                                <div className="data">
                                    <h3>NUEVAS</h3>
                                    <h4>
                                        <strong>{this.state.totalCountNu}</strong>

                                    </h4>
                                </div>
                            </div>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
                <MDBRow>
                    <MDBCol md="6" className="div2">
                        {/* --Card En Espera-- */}
                        <MDBCard className="cascading-admin-card ">
                            <div className="admin-up">
                                <MDBIcon icon="clock" className="amber" />
                                <div className="data">
                                    <h3>EN PROCESO</h3>
                                    <h4>
                                        <strong>{this.state.totalCountPr}</strong>
                                    </h4>
                                </div>
                            </div>

                        </MDBCard>
                    </MDBCol>
                    <MDBCol md="6" className="div2">
                        {/* --Card Rechazadas-- */}
                        <MDBCard className="cascading-admin-card">
                            <div className="admin-up">
                                <MDBIcon icon="times" className="red" />
                                <div className="data">
                                    <h3>RECHAZADAS</h3>
                                    <h4>
                                        <strong>{this.state.totalCountR}</strong>

                                    </h4>
                                </div>
                            </div>

                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </div>
        )
    }
}

export default DashboardEstadoCards;