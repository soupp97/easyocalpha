import React, { Component } from "react";
import { MDBCard, MDBCardHeader, MDBCardBody, MDBIcon, MDBBtn, MDBModal, MDBCol, MDBContainer, MDBInput, MDBModalBody, MDBRow, MDBModalHeader } from "mdbreact";
import { Table, Container } from 'reactstrap';
import firebase from '../../../../Firebase';

class AdminCrudCECO extends Component {

    constructor(props) {
        super(props);
        this.ref = firebase.firestore().collection('area');
        this.state = {
            area:[],
            areaKey: '',
            nm_area: '',//Nombre del área
            key: '',//Key para guardar el ID generado a firebase para el Ceco
            message: ''//Mensaje guardado para mostrar en las validaciones
        };
    }

    state = {
        modalagregarCECO: false,
        modal2: false,
        modal3: false,
        modal4: false,
        modal5: false,
        modal6: false,
        modalagregarArea: false,
        modaleditarArea: false,
        modaleliminarArea: false,
        modalconfirmarEditarArea: false,
        modalconfirmarEliminarArea: false,
        modalmensajeEditadoArea: false,
        modalconfirmarEliminarArea: false
    }

    /*Función para cerrar varios modals para Confirmar la edición del CECO */
    confirmarEditarArea = (tg1, tg2, tg3) => () => {
        let modalName1 = 'modal' + tg1;
        let modalName2 = 'modal' + tg2;
        let modalName3 = 'modal' + tg3;
        this.setState({
            [modalName1]: !this.state[modalName1],
            [modalName2]: !this.state[modalName2],
            [modalName3]: !this.state[modalName3],
        })
    }
    eliminarArea= (ar, tg) => () => {
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            areaKey: ar
        })
    }
 
    /*Función para editar una area en Firebase y usar modal */
    editarArea = (ar, tg) => () => {
        this.ref.doc(ar).get().then((doc) => {
            if (doc.exists) {
                const area = doc.data();
                this.setState({
                    key: doc.id,
                    nm_area: area.nm_area,
                });
            } else {
                console.log("No such document!");
            }
        });
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
        })
    }
    /*Función Sólo para usar un modal */
    toggle = nr => () => {
        let modalName = 'modal' + nr;
        this.setState({
            [modalName]: !this.state[modalName]
        })
    }
    /*Función para limpiar el formulario y usar el modal */
    volver = (nr, tp) => () => {
        let modalName = 'modal' + nr;
        if (tp == "CECO") {
            this.setState({
                key: '',
                nm_ceco: '',
                nombre: '',
                presupuesto: ''
            });
        } else {
            this.setState({
                key2: '',
                nm_area: '',
                nombre_cta: '',
                ceco_key: ''
            });
        }
        this.setState({
            [modalName]: !this.state[modalName],
            message: ''
        })
    }
    /*Función que vigila el estado de un componente  */
    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value
        this.setState(state);
    }
    /*Función que Edita una area en Firebase */
    onSubmitEditarArea = (e) => {
        e.preventDefault();
        /*Agrega la clase de validación de Boostrap */
        e.target.className += "was-validated";
        const { nm_area} = this.state;
        if (nm_area.trim().length <= 0) {
            //Nombre cuetna. inválido
        } else {
            //preguntamos en firebase por la area asociada a la key
            const updateRef = firebase.firestore().collection('area').doc(this.state.key);
            updateRef.set({
                nm_area,
            }).then((docRef) => {
                this.setState({
                    key: '',
                    nm_area: '',
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        }
    }
    /*Función para agregar una area a Firebase */
    onSubmitAgregarArea = (e) => {
        e.preventDefault();
        /*Agrega la clase de validación de Boostrap */
        e.target.className += "was-validated";
        const { nm_area } = this.state;
        if (nm_area.trim().length <= 0) {
            this.setState({
                message: 'Numero de area no válido' //Validación
            });
        } else {
            this.ref.add({
                nm_area,
            }).then((docRef) => {
                this.setState({
                    nm_area: '',
                    message: '',
                    modalagregarArea: !this.state.modalagregarArea
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error); //Validación
                });
        }
    }
    
    //Función para extraer datos actualizados de una area
    onCollectionUpdateArea = (querySnapshot) => {
        const area = [];
        querySnapshot.forEach((doc) => {
            const { nm_area } = doc.data();
            area.push({
                key: doc.id,
                doc, // DocumentSnapshot
                nm_area,
            });
        });
        this.setState({
            area
        });
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdateArea);
    }
    /*Función para eliminar en area en Firebase y usar modals */
    deleteArea(id, tg, tg2) {
        firebase.firestore().collection('area').doc(id).delete().then(() => {
            this.props.history.push("#")
        }).catch((error) => {
        });
        let modalName = 'modal' + tg;
        let modalName2 = 'modal' + tg2;
        this.setState({
            [modalName]: !this.state[modalName],
            [modalName2]: !this.state[modalName2]
        })
    }
    /*Función para eliminar en CECO en Firebase y usar modals */

    render() {
        const {nm_area, areaKey} = this.state;
        return (
            <Container>
                <MDBCard>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <MDBCardHeader><h3>Gestión de Áreas</h3></MDBCardHeader>
                        </div>
                        {/*tabla de areas */}
                        <Table bordered responsive striped>
                            <thead>
                                <tr>
                                    <th>Nombre de Área</th>
                                    <th>Acciones Área</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.area.map(ar =>
                                    <tr>
                                        <td>{ar.nm_area}</td>
                                        <td scope="row">
                                            <MDBBtn color="yellow" onClick={this.editarArea(ar.key, 'editarArea')} ><MDBIcon size="lg" icon="edit" /></MDBBtn>
                                            <MDBBtn color="red" onClick={this.eliminarArea(ar.key, 'eliminarArea')} ><MDBIcon size="lg" icon="times-circle" /></MDBBtn>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        
                        {/*Modal agregar area */}
                        <MDBModal toggle={this.toggle("agregarArea")} isOpen={this.state.modalagregarArea} size="md" backdrop={false}>
                            <MDBModalHeader>Ingrese los datos de la area</MDBModalHeader>
                            <MDBModalBody >
                                <form noValidate onSubmit={this.onSubmitAgregarArea}>
                                    <MDBRow>
                                        <MDBCol>
                                            <div className="form-group">
                                                <MDBInput label="Nombre de area" outline type="text" required className="form-control" name="nm_area" value={nm_area} onChange={this.onChange} placeholder="Nombre area" ></MDBInput>
                                            </div>
                                            <h6 style={{ fontSize: '12px', color: 'red' }}>{this.state.message}</h6>
                                        </MDBCol>
                                    </MDBRow>
                                    {/*Botones Agregar*/}
                                    <MDBRow>
                                        <MDBCol className="text-center">
                                            <MDBBtn type="submit" color="green" size="md">Agregar</MDBBtn>
                                            <MDBBtn color="primary" size="md" onClick={this.volver("agregArarea", "area")}>Volver</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>
                                </form>




                            </MDBModalBody>
                        </MDBModal>
                        {/*Modal eliminar area */}
                        <MDBModal className="pl-5" size="sm" toggle={this.toggle()} isOpen={this.state.modaleliminarArea} backdrop={true}>
                            <MDBModalHeader>¿Desea eliminarlo?</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer>
                                    Esta acción es irreversible
                                <br></br>
                                    <br></br>
                                    <MDBCol>
                                        <MDBBtn color="red" size="md" onClick={this.deleteArea.bind(this, areaKey , "eliminarArea", "confirmarEliminarArea")} >Si</MDBBtn>
                                        <MDBBtn color="blue" size="md" onClick={this.volver("eliminarArea", "area")}>No</MDBBtn>
                                    </MDBCol>
                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>
                        
                        {/*Botón para Agregar */}
                        <MDBBtn size="md" md="4" block color="green" onClick={this.toggle('agregarArea')}>Agregar Area</MDBBtn>
                        
                        {/*Modal Editar area */}
                        <MDBModal toggle={this.toggle("editarArea")} isOpen={this.state.modaleditarArea} size="md" backdrop={false}>
                            <MDBModalHeader>Modifique los datos de la area</MDBModalHeader>
                            <MDBModalBody >
                                <MDBCol >
                                    <form noValidate onSubmit={this.onSubmitEditarArea}>
                                        <MDBRow>
                                            <MDBCol>
                                                <div className="form-group">
                                                    <MDBInput label="Numero de area" outline type="text" required className="form-control" name="nm_area" value={nm_area} onChange={this.onChange} placeholder="Numero area" ></MDBInput>
                                                </div>
                                            </MDBCol>
                                        </MDBRow>
                                        {/*Botones Agregar Area */}
                                        <MDBRow>
                                            <MDBCol className="text-center">
                                                <MDBBtn color="warning" size="md" onClick={this.toggle("confirmarEditarArea")}>Editar</MDBBtn>
                                                <MDBBtn color="primary" size="md" onClick={this.volver("editarArea", "area")}>Volver</MDBBtn>
                                            </MDBCol>
                                        </MDBRow>
                                        {/*Sección Modal Confirmar Editar */}
                                        <MDBModal className="pl-5" size="sm" toggle={this.toggle("confirmarEditarArea")} isOpen={this.state.modalconfirmarEditarArea} backdrop={true}>
                                            <MDBModalHeader>¿Está seguro?</MDBModalHeader>
                                            <MDBModalBody>
                                                <MDBContainer center>
                                                    ¿Está seguro que desea editar esta Area?
                                                    <br></br>
                                                    <br></br>
                                                    <MDBCol >
                                                        <MDBBtn type="submit" color="warning" size="md" onClick={this.confirmarEditarArea("editarArea", "confirmarEditarArea", "mensajeEditadoArea")} >Si</MDBBtn>
                                                        <MDBBtn color="primary" size="md" onClick={this.toggle("confirmarEditarArea")}>No</MDBBtn>
                                                    </MDBCol>
                                                </MDBContainer>
                                            </MDBModalBody>
                                        </MDBModal>

                                    </form>
                                </MDBCol>
                            </MDBModalBody>
                        </MDBModal>
                        
                    </div>
                </MDBCard>
                <MDBModal size="sm" toggle={this.toggle("mensajeEditadoArea")} isOpen={this.state.modalmensajeEditadoArea} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#FBB138' }}><MDBIcon icon="exclamation-triangle" />   Area Editada Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                <MDBModal toggle={this.toggle("confirmarEliminarArea")} isOpen={this.state.modalconfirmarEliminarArea} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#C51E1E' }}><MDBIcon icon="user-times" />   Area Eliminada Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
            </Container>
        );
    }
}
export default AdminCrudCECO;