//Importaciones de los módulos para usar en esta sección
import React, { Component } from 'react';
import { Pie, Line } from "react-chartjs-2";
import { MDBCard, MDBRow, MDBContainer, MDBCol, MDBCardText } from 'mdbreact';
//Clase que contendrá la base del DashBoard
class ReportesSection extends React.Component {
    state = {
        dataLine: {
            labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio"],
            datasets: [
                {
                    label: "Área 1",
                    fill: true,
                    lineTension: 0.3,
                    backgroundColor: "rgba(225, 204,230, .3)",
                    borderColor: "rgb(205, 130, 158)",
                    borderCapStyle: "butt",
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: "miter",
                    pointBorderColor: "rgb(205, 130,1 58)",
                    pointBackgroundColor: "rgb(255, 255, 255)",
                    pointBorderWidth: 10,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(0, 0, 0)",
                    pointHoverBorderColor: "rgba(220, 220, 220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: "Área 2",
                    fill: true,
                    lineTension: 0.3,
                    backgroundColor: "rgba(184, 185, 210, .3)",
                    borderColor: "rgb(35, 26, 136)",
                    borderCapStyle: "butt",
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: "miter",
                    pointBorderColor: "rgb(35, 26, 136)",
                    pointBackgroundColor: "rgb(255, 255, 255)",
                    pointBorderWidth: 10,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(0, 0, 0)",
                    pointHoverBorderColor: "rgba(220, 220, 220, 1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]
        },
        dataPie: {
            labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
            datasets: [
                {
                    data: [300, 50, 100, 40, 120],
                    backgroundColor: [
                        "#F7464A",
                        "#46BFBD",
                        "#FDB45C",
                        "#949FB1",
                        "#4D5360",
                        "#AC64AD"
                    ],
                    hoverBackgroundColor: [
                        "#FF5A5E",
                        "#5AD3D1",
                        "#FFC870",
                        "#A8B3C5",
                        "#616774",
                        "#DA92DB"
                    ]
                }
            ]
        }
    }
    render() {
        return (
            <MDBContainer fluid>
                <MDBRow className="div2 mt-4" style={{ margin: "-30px" }}>
                    <MDBCol>

                        <MDBCard className="div2" >
                            <MDBRow>
                                <MDBCol>
                                    <div className="mt-3 div2 text-center">
                                        <h2>Reportes</h2>
                                    </div>
                                </MDBCol>

                            </MDBRow>

                            <MDBRow>

                                <MDBCol md="8">
                                    <MDBContainer className="text-center">
                                        <Pie data={this.state.dataPie} options={{ responsive: true }} />
                                        <select className="browser-default custom-select mt-1">
                                            <option value="0" className="text-center">Ordenes de Compra</option>
                                            <option value="1">Reembolsos</option>
                                            <option value="2">Sumas por Rendir</option>
                                        </select>
                                    </MDBContainer>
                                </MDBCol>

                                <MDBCol md="4">
                                    <MDBContainer className="text-center">
                                        <h6>Filtros</h6>
                                        <select className="browser-default custom-select mt-1">
                                            <option defaultValue="0">Seleccione Área</option>
                                            <option value="1">Área 1</option>
                                            <option value="2">Área 2</option>
                                            <option value="3">Área 3</option>
                                        </select>
                                        <select className="browser-default custom-select mt-1">
                                            <option defaultValue="0">Proveedor</option>
                                            <option value="1">Proveedor 1</option>
                                            <option value="2">Proveedor 2</option>
                                            <option value="3">Proveedor 3</option>
                                        </select>

                                        <select className="browser-default custom-select mt-1">
                                            <option defaultValue="0">Estado</option>
                                            <option value="1">Todas</option>
                                            <option value="2">En Proceso</option>
                                            <option value="3">Vencidas</option>
                                            <option value="4">Finalizada</option>
                                        </select>
                                        <MDBRow>
                                            <MDBCol>
                                                <select className="browser-default custom-select mt-1">
                                                    <option defaultValue="0">Fecha desde</option>
                                                    <option value="1">One</option>
                                                </select>
                                            </MDBCol>
                                            <MDBCol>
                                                <select className="browser-default custom-select mt-1">
                                                    <option defaultValue="0">Fecha Hasta</option>
                                                    <option value="1">One</option>
                                                </select>
                                            </MDBCol>
                                        </MDBRow>
                                        <MDBRow>
                                            <MDBCol>
                                                <select className="browser-default custom-select mt-1">
                                                    <option defaultValue="0">Monto desde</option>
                                                    <option value="1">One</option>
                                                </select>
                                            </MDBCol>
                                            <MDBCol>
                                                <select className="browser-default custom-select mt-1">
                                                    <option defaultValue="0">Monto Hasta</option>
                                                    <option value="1">One</option>
                                                </select>
                                            </MDBCol>
                                        </MDBRow>

                                    </MDBContainer>
                                </MDBCol>
                            </MDBRow>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
                <MDBRow className="div2 mt-4" style={{ margin: "-30px" }}>
                    <MDBCol>
                        <MDBCard className="div2" >
                            <MDBRow>
                                <MDBCol>
                                    <MDBContainer className="text-center">
                                        <h2 className="mt-3 div2">Gráfico de Lineas</h2>
                                        <Line data={this.state.dataLine} options={{ responsive: true }} />
                                    </MDBContainer>
                                </MDBCol>
                            </MDBRow>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>

        )
    }
}

export default ReportesSection;

