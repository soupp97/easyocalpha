import React, { Component } from "react";
import { MDBDataTable, MDBCard, MDBCardHeader, MDBCardBody, MDBIcon, MDBBtn, MDBModal, MDBCol, MDBContainer, MDBInput, MDBModalBody, MDBRow, MDBModalHeader } from "mdbreact";
import { Table, Container } from 'reactstrap';
import firebase from '../../../../Firebase';
import Firebase from "../../../../Firebase";

class AdminCrudCECO extends Component {
    ceco_sel = [];//Ceco seleccionado para guardar las cuentas especificas de este ceco
    
    constructor(props) {
        super(props);
        this.ref = firebase.firestore().collection('centro_costo');
        this.ref2 = firebase.firestore().collection('cuenta');
        this.state = {
            ceco: [], //Array de CECOS
            nro_ceco: '', //Numero de Ceco
            nombre: '', //Nombre de Ceco
            presupuesto: '',//Presupuesto del Ceco
            cuentas: [],//Array de cuentas asociadas a un ceco
            nro_cuenta: '',//Numero de Cuenta
            nombre_cta: '',//Nombre de Cuenta
            ceco_key: '',//Key de CECO el cual sirve para asociar una cuenta a un ceco
            key: '',//Key para guardar el ID generado a firebase para el Ceco
            key2: '',//Key para guardar el ID generado a firebase para la Cuenta
            message:''//Mensaje guardado para mostrar en las validaciones
        };
    }

    state = {
        cec: '',
        cta: '',
        modalagregarCECO: false,
        modal2: false,
        modal3: false,
        modal4: false,
        modal5: false,
        modal6: false,
        modalverCtas: false,
        modalagregarCta: false,
        modalagregarCuenta: false,
        modaleditarCta: false,
        modaleliminarCta: false,
        modalconfirmarEditarCta:false,
        modalconfirmarEliminarCta:false,
        modalmensajeEditadoCta:false,
        modalconfirmarEliminarCta:false
    }
    /*Aquí no nos dejó llamar al "toggle" por lo cual debimos de redundar código para hacer que el modal se abrá y se cierre */
    verCtas = (ce, tg) => () => {
        let modalName = 'modal' + tg;
        this.ceco_sel = ce;
        /*Función para que el modal se abra y para guardar la key de una CECO */
        this.setState({
            [modalName]: !this.state[modalName],
            ceco_key: this.ceco_sel.key
        });
    }
    /*Función para cerrar varios modals para Confirmar la edición del CECO */
    confirmarEditarCECO = (tg1, tg2, tg3) => () => {
        let modalName1 = 'modal' + tg1;
        let modalName2 = 'modal' + tg2;
        let modalName3 = 'modal' + tg3;
        this.setState({
            [modalName1]: !this.state[modalName1],
            [modalName2]: !this.state[modalName2],
            [modalName3]: !this.state[modalName3],
        })
    }
    /*Función para cerrar varios modals para Confirmar la edición de la Cuenta */
    confirmarEditarCuenta = (tg1, tg2) => () => {
        let modalName1 = 'modal' + tg1;
        let modalName2 = 'modal' + tg2;
        this.setState({
            [modalName1]: !this.state[modalName1],
            [modalName2]: !this.state[modalName2]
        })
    }
    /*Función para modal de Eliminar y rescatar el key del CECO */
    eliminarCECO = (ceco1, tg) => () => {
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            cec: ceco1
        })
    }
    /*Función para modal de Eliminar y rescatar el key de la Cuenta */
    eliminarCuenta = (cta1, tg) => () => {
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
            cta: cta1
        })
    }
    /*Función para editar una cuenta en Firebase y usar modal */
    editarCuenta = (_cta, tg) => () => {
        this.ref2.doc(_cta).get().then((doc) => {
            if (doc.exists) {
                const ct = doc.data();
                this.setState({
                    key2: doc.id,
                    nro_cuenta: ct.nro_cuenta,
                    nombre_cta: ct.nombre_cta,
                    ceco_key:ct.ceco_key
                });
            } else {
                console.log("No such document!");
            }
        });
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
        })
    }
    /*Función para editar una cuenta en Firebase y usar modal */
    editarCECO = (_ceco, tg) => () => {
        const ref = firebase.firestore().collection('centro_costo').doc(_ceco);
        ref.get().then((doc) => {
            if (doc.exists) {
                const ceco = doc.data();
                this.setState({
                    key: doc.id,
                    nro_ceco: ceco.nro_ceco,
                    nombre: ceco.nombre,
                    presupuesto: ceco.presupuesto
                });
            } else {
                console.log("No such document!");
            }
        });
        let modalName = 'modal' + tg;
        this.setState({
            [modalName]: !this.state[modalName],
        })
    }
    /*Función Sólo para usar un modal */
    toggle = nr => () => {
        let modalName = 'modal' + nr;
        this.setState({
            [modalName]: !this.state[modalName]
        })
    }
    /*Función para limpiar el formulario y usar el modal */
    volver = (nr,tp) => () =>{
        let modalName = 'modal' + nr;
        if(tp=="CECO"){
            this.setState({
                key: '',
                nro_ceco: '',
                nombre: '',
                presupuesto: ''
            });
        }else{
            this.setState({
                    key2: '',
                    nro_cuenta: '',
                    nombre_cta: '',
                    ceco_key: ''
                });
        }
        this.setState({
            [modalName]: !this.state[modalName],
            message:''
        })
    }
    /*Función que vigila el estado de un componente  */
    onChange = (e) => {
        const state = this.state
        if (e.target.name == 'nro_ceco') {
            state[e.target.name] = e.target.value
            this.setState(state);
        } else {
            state[e.target.name] = e.target.value
            this.setState(state);
        }

    }
    /*Función que Edita una cuenta en Firebase */
    onSubmitEditarCuenta = (e) => {
        e.preventDefault();
        /*Agrega la clase de validación de Boostrap */
        e.target.className += "was-validated";

        const { nro_cuenta, nombre_cta, ceco_key } = this.state;
        if (nro_cuenta.trim().length <= 0) {
            //Nro cta. inválido
        } else if (nombre_cta.trim().length <= 0) {
            //nombre cta inválido
        }else{
            //preguntamos en firebase por la cuenta asociada a la key
            const updateRef = firebase.firestore().collection('cuenta').doc(this.state.key2);
            updateRef.set({
                nro_cuenta,
                nombre_cta,
                ceco_key
            }).then((docRef) => {
                this.setState({
                    key2: '',
                    nro_cuenta: '',
                    nombre_cta: ''
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        }
    }
    //Función para Editar en Firebase
    onSubmitEditarCECO = (e) => {
        e.preventDefault();
        /*Agrega la clase de validación de Boostrap */
        e.target.className += "was-validated";
        const { nro_ceco, nombre, presupuesto } = this.state;
        if (nro_ceco.trim().length <= 0) {
            //validación
        } else if (nombre.trim().length <= 0) {
            //validación
        } else if (presupuesto.trim().length <= 0) {
            //validación
        } else {
            //preguntamos en firebase por la cuenta asociada a la key
            const updateRef = firebase.firestore().collection('centro_costo').doc(this.state.key);
            updateRef.set({
                nro_ceco,
                nombre,
                presupuesto
            }).then((docRef) => {
                this.setState({
                    key: '',
                    nro_ceco: '',
                    nombre: '',
                    presupuesto: ''
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        }
    }
    /*Función para agregar una cuenta a Firebase */
    onSubmitAgregarCuenta = (e) => {
        e.preventDefault();
        /*Agrega la clase de validación de Boostrap */
        e.target.className += "was-validated";
        const { nro_cuenta, nombre_cta, ceco_key } = this.state;
        if (nro_cuenta.trim().length <= 0) {
            this.setState({
                message:'Numero de cuenta no válido' //Validación
            });
        } else if (nombre_cta.trim().length <= 0) {
            this.setState({
                message:'Nombre de cuenta no válido' //Validación
            });
        } else {
            this.ref2.add({
                nro_cuenta,
                nombre_cta,
                ceco_key
            }).then((docRef) => {
                this.setState({
                    nro_cuenta: '',
                    nombre_cta: '',
                    message:'',
                    modalagregarCuenta:!this.state.modalagregarCuenta
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error); //Validación
                });        }
    }
    //Función para Agregar CECO en Firebase
    onSubmitAgregarCECO = (e) => {
        e.preventDefault();
        e.target.className += "was-validated";
        const { nro_ceco, nombre, presupuesto } = this.state;
        if (nro_ceco.trim().length <= 0) {
            //Validación
        } else if (nombre.trim().length <= 0) {
            //Validación
        } else if (presupuesto.trim().length <= 0) {
            //Validación
        } else {
            this.ref.add({
                nro_ceco,
                nombre,
                presupuesto
            }).then((docRef) => {
                this.setState({
                    nro_ceco: '',
                    nombre: '',
                    presupuesto: ''
                });
                this.props.history.push("#")
            })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        }
    }
    //Función para extraer datos actualizados de CECO
    onCollectionUpdate = (querySnapshot) => {
        const ceco = [];
        querySnapshot.forEach((doc) => {
            const { nro_ceco, nombre, presupuesto } = doc.data();
            ceco.push({
                key: doc.id,
                doc, // DocumentSnapshot
                nro_ceco,
                nombre,
                presupuesto
            });
        });
        this.setState({
            ceco
        });
    }
    //Función para extraer datos actualizados de una Cuenta
    onCollectionUpdate2 = (querySnapshot) => {
        const cuentas = [];
        querySnapshot.forEach((doc) => {
            const { nro_cuenta, nombre_cta, ceco_key } = doc.data();
            cuentas.push({
                key: doc.id,
                doc, // DocumentSnapshot
                nro_cuenta,
                nombre_cta,
                ceco_key
            });
        });
        this.setState({
            cuentas
        });
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
        this.unsubscribe = this.ref2.onSnapshot(this.onCollectionUpdate2);
        setInterval(this.verCtas, 1000);
    }
    /*Función para eliminar en Cuenta en Firebase y usar modals */
    deleteCuenta(id, tg, tg2) {
        firebase.firestore().collection('cuenta').doc(id).delete().then(() => {
            this.props.history.push("#")
        }).catch((error) => {
        });
        let modalName = 'modal' + tg;
        let modalName2 = 'modal' + tg2;
        this.setState({
            [modalName]: !this.state[modalName],
            [modalName2]: !this.state[modalName2]
        })
    }
    /*Función para eliminar en CECO en Firebase y usar modals */
    deleteCECO(id, tg, tg2) {
        this.state.cuentas.forEach((f) => {
            if (f.ceco_key == id) {
                this.ref2.doc(f.key).delete().then(() => {
                    this.props.history.push("#")
                }).catch((error) => {
                });
            }
        })
        firebase.firestore().collection('centro_costo').doc(id).delete().then(() => {
            this.props.history.push("#")
        }).catch((error) => {
        });
        let modalName = 'modal' + tg;
        let modalName2 = 'modal' + tg2;
        this.setState({
            [modalName]: !this.state[modalName],
            [modalName2]: !this.state[modalName2]
        })
    }

    render() {
        const { nro_ceco, nombre, presupuesto, nro_cuenta, nombre_cta, cta, cec } = this.state;
        return (
            <Container>
                <MDBCard>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <MDBCardHeader><h3>Gestión de Centro de Costos</h3></MDBCardHeader>
                        </div>
                        {/*tabla de cecos */}
                        <Table bordered responsive striped>
                            <thead>
                                <tr>
                                    <th>Numero CECO</th>
                                    <th>Nombre</th>
                                    <th>Presupuesto</th>
                                    <th>Ver Cuentas</th>
                                    <th>Acciones CECO</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.ceco.map(ceco =>
                                    <tr>
                                        <td>{ceco.nro_ceco}</td>
                                        <td>{ceco.nombre}</td>
                                        <td>$ {ceco.presupuesto}</td>
                                        <td><MDBBtn color="primary" onClick={this.verCtas(ceco, "verCtas")} >Ver Cuentas  <MDBIcon className="ml-2" size="lg" icon="list-alt" /></MDBBtn></td>
                                        <td scope="row">
                                            <MDBBtn color="yellow" onClick={this.editarCECO(ceco.key, 2)} ><MDBIcon size="lg" icon="edit" /></MDBBtn>
                                            <MDBBtn color="red" onClick={this.eliminarCECO(ceco.key, 5)} ><MDBIcon size="lg" icon="times-circle" /></MDBBtn>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        {/*Modal ver Cuentas */}
                        <MDBModal className="pl-5" size="lg" toggle={this.toggle("verCtas")} isOpen={this.state.modalverCtas} backdrop={true}>
                            <MDBModalHeader>Ver Cuentas Asociadas</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer>
                                    <Table bordered responsive striped>
                                        <thead>
                                            <tr>
                                                <th>Numero Cuenta</th>
                                                <th>Nombre Cuenta</th>
                                                <th>CECO Asociado</th>
                                                <th>Acciones Cuenta</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.cuentas.map(ct =>
                                                (this.ceco_sel != null ? (ct.ceco_key == this.ceco_sel.key ?
                                                    <tr>
                                                        <td>{ct.nro_cuenta}</td>
                                                        <td>{ct.nombre_cta}</td>
                                                        <td>{this.ceco_sel.nro_ceco} {this.ceco_sel.nombre}</td>
                                                        <td scope="row">
                                                            <MDBBtn color="yellow" onClick={this.editarCuenta(ct.key, "editarCta")} ><MDBIcon size="lg" icon="edit" /></MDBBtn>
                                                            <MDBBtn color="red" onClick={this.eliminarCuenta(ct.key, "eliminarCta")}><MDBIcon size="lg" icon="times-circle" /></MDBBtn>
                                                        </td>
                                                    </tr>
                                                    : null) : null)
                                            )}
                                        </tbody>
                                    </Table>
                                    <MDBRow middle="true">
                                        <MDBCol className="mx-2">
                                            <MDBBtn block color="blue" size="md" onClick={this.toggle("agregarCuenta")}>Agregar Cuenta</MDBBtn>
                                        </MDBCol>

                                        <MDBCol className="mx-2">

                                            <MDBBtn block color="blue" size="md" onClick={this.volver("verCtas","Cuenta")}>Volver</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>

                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>
                        {/*Modal agregar Cuentas */}
                        <MDBModal toggle={this.toggle("agregarCuenta")} isOpen={this.state.modalagregarCuenta} size="md" backdrop={false}>
                            <MDBModalHeader>Ingrese los datos de la cuenta</MDBModalHeader>
                            <MDBModalBody >
                                <form noValidate onSubmit={this.onSubmitAgregarCuenta}>
                                    <MDBRow>
                                        <MDBCol>
                                            <div className="form-group">
                                                <MDBInput label="Numero de Cuenta" outline type="number" required className="form-control" name="nro_cuenta" value={nro_cuenta} onChange={this.onChange} placeholder="Numero Cuenta" ></MDBInput>
                                            </div>
                                            <div className="form-group">
                                                <MDBInput label="Nombre" outline required className="form-control" name="nombre_cta" onChange={this.onChange} value={nombre_cta} placeholder="Nombre" ></MDBInput>
                                            </div>
                                            <h6 style={{fontSize:'12px',color:'red'}}>{this.state.message}</h6>
                                        </MDBCol>


                                    </MDBRow>
                                    {/*Botones Agregar*/}
                                    <MDBRow>
                                        <MDBCol className="text-center">
                                            <MDBBtn type="submit" color="green" size="md">Agregar</MDBBtn>
                                            <MDBBtn color="primary" size="md" onClick={this.volver("agregarCuenta","Cuenta")}>Volver</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>
                                </form>




                            </MDBModalBody>
                        </MDBModal>
                        {/*Modal eliminar Cuenta */}
                        <MDBModal className="pl-5" size="sm" toggle={this.toggle()} isOpen={this.state.modaleliminarCta} backdrop={true}>
                            <MDBModalHeader>¿Desea eliminarlo?</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer>
                                    Esta acción es irreversible
                                <br></br>
                                    <br></br>
                                    <MDBCol>
                                        <MDBBtn color="red" size="md" onClick={this.deleteCuenta.bind(this, cta, "eliminarCta", "confirmarEliminarCta")} >Si</MDBBtn>
                                        <MDBBtn color="blue" size="md" onClick={this.volver("eliminarCta","Cuenta")}>No</MDBBtn>
                                    </MDBCol>
                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>
                        {/*-Sección Modal Eliminar */}
                        <MDBModal size="md" toggle={this.toggle()} isOpen={this.state.modal5} backdrop={true}>
                            <MDBModalHeader>¿Desea eliminar esto?</MDBModalHeader>
                            <MDBModalBody>
                                <MDBContainer fluid>
                                    <MDBRow middle="true" className="mb-5">
                                        <MDBCol>
                                            <h5 style={{ fontSize: '17px' }}>Eliminar un CECO impone:</h5>
                                            <h5 style={{ color: 'red', fontSize: '17px' }}>Eliminar <strong>TODAS</strong> las cuentas asociadas a este CECO.</h5>
                                            <h5 style={{ fontSize: '17px' }}>Sabiendo esto, ¿Continuar con la eliminación?</h5>
                                        </MDBCol>
                                    </MDBRow>

                                    <MDBRow middle="true">
                                        <MDBCol className="mx-2">
                                            <MDBBtn block color="red" size="md" onClick={this.deleteCECO.bind(this, cec, 5, 6)} >Si</MDBBtn>
                                        </MDBCol>
                                        <MDBCol className="mx-2">
                                            <MDBBtn block color="blue" size="md" onClick={this.volver("5","CECO")}>No</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>

                                </MDBContainer>
                            </MDBModalBody>
                        </MDBModal>

                        {/*Botón para Agregar */}
                        <MDBBtn size="md" md="4" block color="green" onClick={this.toggle('agregarCECO')}>Agregar CECO</MDBBtn>
                        {/*-Sección Modal Agregar */}
                        <MDBModal toggle={this.toggle()} isOpen={this.state.modalagregarCECO} size="md" backdrop={false}>
                            <MDBModalHeader>Ingrese los datos del CECO</MDBModalHeader>
                            <MDBModalBody >

                                <form noValidate onSubmit={this.onSubmitAgregarCECO}>
                                    <MDBRow>
                                        <MDBCol>
                                            <div className="form-group">
                                                
                                                <MDBInput label="Numero de CECO" outline type="number" required className="form-control" name="nro_ceco" value={nro_ceco} onChange={this.onChange}  ></MDBInput>
                                            </div>
                                            <div className="form-group">
                                                
                                                <MDBInput label="Nombre" outline required className="form-control" name="nombre" onChange={this.onChange} value={nombre} ></MDBInput>
                                            </div>
                                            <div className="form-group">
                                                
                                                <MDBInput label="Presupuesto" outline required type="number" className="form-control" name="presupuesto" onChange={this.onChange} value={presupuesto} ></MDBInput>
                                            </div>
                                        </MDBCol>


                                    </MDBRow>
                                    {/*Botones Agregar*/}
                                    <MDBRow>
                                        <MDBCol className="text-center">
                                            <MDBBtn type="submit" color="green" size="md" onClick={this.toggle('agregarCECO')}>Agregar</MDBBtn>
                                            <MDBBtn color="primary" size="md" onClick={this.volver('agregarCECO',"CECO")}>Volver</MDBBtn>
                                        </MDBCol>
                                    </MDBRow>
                                </form>




                            </MDBModalBody>
                        </MDBModal>
                        {/*Modal Editar Cuenta */}
                        <MDBModal toggle={this.toggle("editarCta")} isOpen={this.state.modaleditarCta} size="md" backdrop={false}>
                            <MDBModalHeader>Modifique los datos de la Cuenta</MDBModalHeader>
                            <MDBModalBody >
                                <MDBCol >
                                    <form noValidate onSubmit={this.onSubmitEditarCuenta}>
                                        <MDBRow>
                                            <MDBCol>
                                            <div className="form-group">
                                                <MDBInput label="Numero de Cuenta" outline type="number" required className="form-control" name="nro_cuenta" value={nro_cuenta} onChange={this.onChange} placeholder="Numero Cuenta" ></MDBInput>
                                            </div>
                                            <div className="form-group">
                                                <MDBInput label="Nombre" outline required className="form-control" name="nombre_cta" onChange={this.onChange} value={nombre_cta} placeholder="Nombre" ></MDBInput>
                                            </div>
                                            </MDBCol>
                                        </MDBRow>
                                        {/*Botones Agregar CECO */}
                                        <MDBRow>
                                            <MDBCol className="text-center">
                                                <MDBBtn color="warning" size="md" onClick={this.toggle("confirmarEditarCta")}>Editar</MDBBtn>
                                                <MDBBtn color="primary" size="md" onClick={this.volver("editarCta","Cuenta")}>Volver</MDBBtn>
                                            </MDBCol>
                                        </MDBRow>
                                        {/*Sección Modal Confirmar Editar */}
                                        <MDBModal className="pl-5" size="sm" toggle={this.toggle("confirmarEditarCta")} isOpen={this.state.modalconfirmarEditarCta} backdrop={true}>
                                            <MDBModalHeader>¿Está seguro?</MDBModalHeader>
                                            <MDBModalBody>
                                                <MDBContainer center>
                                                    ¿Está seguro que desea editar esta Cuenta?
                                                    <br></br>
                                                    <br></br>
                                                    <MDBCol >
                                                        <MDBBtn type="submit" color="warning" size="md" onClick={this.confirmarEditarCECO("editarCta","confirmarEditarCta","mensajeEditadoCta")} >Si</MDBBtn>
                                                        <MDBBtn color="primary" size="md" onClick={this.toggle("confirmarEditarCta")}>No</MDBBtn>
                                                    </MDBCol>
                                                </MDBContainer>
                                            </MDBModalBody>
                                        </MDBModal>

                                    </form>
                                </MDBCol>
                            </MDBModalBody>
                        </MDBModal>
                        {/*Sección Modal Editar */}
                        <MDBModal toggle={this.toggle(2)} isOpen={this.state.modal2} size="md" backdrop={false}>
                            <MDBModalHeader>Ingrese los datos del CECO</MDBModalHeader>
                            <MDBModalBody >
                                <MDBCol >
                                    <form noValidate onSubmit={this.onSubmitEditarCECO}>
                                        <MDBRow>
                                            <MDBCol>
                                            <div className="form-group">
                                                
                                                <MDBInput label="Numero de CECO" outline type="number" required className="form-control" name="nro_ceco" value={nro_ceco} onChange={this.onChange}  ></MDBInput>
                                            </div>
                                            <div className="form-group">
                                                
                                                <MDBInput label="Nombre" outline required className="form-control" name="nombre" onChange={this.onChange} value={nombre} ></MDBInput>
                                            </div>
                                            <div className="form-group">
                                                
                                                <MDBInput label="Presupuesto" outline required type="number" className="form-control" name="presupuesto" onChange={this.onChange} value={presupuesto} ></MDBInput>
                                            </div>
                                                <h2>{this.state.message}</h2>
                                            </MDBCol>
                                        </MDBRow>
                                        {/*Botones Agregar*/}
                                        <MDBRow>
                                            <MDBCol className="text-center">
                                                <MDBBtn color="warning" size="md" onClick={this.toggle(3)}>Editar</MDBBtn>
                                                <MDBBtn color="primary" size="md" onClick={this.volver("2","CECO")}>Volver</MDBBtn>
                                            </MDBCol>
                                        </MDBRow>
                                        {/*Sección Modal Confirmar Editar */}
                                        <MDBModal className="pl-5" size="sm" toggle={this.toggle(3)} isOpen={this.state.modal3} backdrop={true}>
                                            <MDBModalHeader>¿Está seguro?</MDBModalHeader>
                                            <MDBModalBody>
                                                <MDBContainer center>
                                                    Está seguro que desea editar este CECO?
                                                    <br></br>
                                                    <br></br>
                                                    <MDBCol >
                                                        <MDBBtn type="submit" color="warning" size="md" onClick={this.confirmarEditarCECO(2, 3, 4)} >Si</MDBBtn>
                                                        <MDBBtn color="primary" size="md" onClick={this.toggle(3)}>No</MDBBtn>
                                                    </MDBCol>
                                                </MDBContainer>
                                            </MDBModalBody>
                                        </MDBModal>

                                    </form>
                                </MDBCol>
                            </MDBModalBody>
                        </MDBModal>
                    </div>
                </MDBCard>
                <MDBModal size="sm" toggle={this.toggle(4)} isOpen={this.state.modal4} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#FBB138' }}><MDBIcon icon="exclamation-triangle" />   CECO Editado Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                <MDBModal size="sm" toggle={this.toggle("mensajeEditadoCta")} isOpen={this.state.modalmensajeEditadoCta} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#FBB138' }}><MDBIcon icon="exclamation-triangle" />   Cuenta Editada Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                <MDBModal toggle={this.toggle(6)} isOpen={this.state.modal6} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#C51E1E' }}><MDBIcon icon="user-times" />   CECO Eliminado Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
                <MDBModal toggle={this.toggle("confirmarEliminarCta")} isOpen={this.state.modalconfirmarEliminarCta} position="top" frame>
                    <MDBModalBody className="text-center">
                        <p style={{ color: '#C51E1E' }}><MDBIcon icon="user-times" />   Cuenta Eliminada Satisfactoriamente</p>
                    </MDBModalBody>
                </MDBModal>
            </Container>
        );
    }
}
export default AdminCrudCECO;