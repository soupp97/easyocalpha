import React, { Component } from "react";
import { MDBBtn, MDBTable, MDBTableBody, MDBCardBody, MDBCard, MDBIcon, MDBRow, MDBCol, MDBCardTitle } from 'mdbreact';
import Firebase from '../../../../Firebase';
import "react-datepicker/dist/react-datepicker.css";
import NumberFormat from 'react-number-format'; //Libreria para formatear presupuesto


class ListaSolicitudDashboard extends Component {
  constructor(props) {
    super(props);
    this.refSol = Firebase.firestore().collection('solicitud').orderBy('fecha_emision', 'desc');
    this.refUs = Firebase.firestore().collection('usuario');
    this.unsubscribe = null;
    this.state = {
      solicitud: [],
      usuario: [],
      nombres: '',
      apellidos: '',
      cargo: '',
      desc_sol: '',
      tipo_sol: '',
      total_sol: '',
      estadoSol: 'Todas',
      fecha_emision: {},
      estado: '',
      usuKey: '',
      area_sol: '',
      estadoVal: '',
      startDate: new Date()
    };
    this.handleChange = this.handleChange.bind(this);

  }

  componentDidMount() {
    this.unsubscribe = this.refUs.onSnapshot(this.onCollectionUpdateUs);
    this.unsubscribe = this.refSol.onSnapshot(this.onCollectionUpdate);
    console.log(this.props)
  }

  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value
    this.setState(state);
    console.log(e.target.value);
  }


  onCollectionUpdateUs = (querySnapshot) => {
    const usuario = [];
    querySnapshot.forEach((doc) => {
      const { nombres, apellidos, cargo } = doc.data();
      usuario.push({
        key: doc.id,
        doc, // DocumentSnapshot
        nombres,
        cargo,
        apellidos
      });
    });
    this.setState({
      usuario
    });
  }

  onCollectionUpdate = (querySnapshot) => {
    const solicitud = [];
    const currUs = Firebase.auth().currentUser.uid;
    querySnapshot.forEach((doc) => {
      const { desc_sol, tipo_sol, total_sol, fecha_emision, estadoVal, estado, usuKey, area_sol } = doc.data();//Si el usuario es Admin  puede ver todas las solicitudes
      solicitud.push({
        key: doc.id,
        doc, // DocumentSnapshot
        desc_sol,
        tipo_sol,
        total_sol,
        fecha_emision,
        estado,
        estadoVal,
        usuKey,
        area_sol
      })
      this.setState({
        solicitud
      });
    });
  }



  handleChange(date) {
    this.setState({
      startDate: date
    });
  }
  render() {

    return (
      <MDBCard>
        <MDBCardBody>
          <MDBCard className="px-2 pt-2 mb-2">
          <MDBRow>
            <MDBCol className="col-lg-6">
              <MDBCardTitle className="mt-1">Filtrar Solicitudes por Estado:</MDBCardTitle>
            </MDBCol>
            <MDBCol className="col-lg-6"> <select  name="estadoSol" onChange={this.onChange} className="browser-default custom-select">
              <option value="Todas">Todas</option>
              <option value="Por Validar">Por Validar</option>
              <option value="Validada">Validada</option>
              <option value="Generada">Generada</option>
              <option value="Liberada">Liberada</option>
              <option value="Completada">Completada</option>
              <option value="Confirmada">Confirmada</option>
              <option value="Rechazada">Rechazada</option>
            </select></MDBCol>




          </MDBRow>
          </MDBCard>
          <div className="scrollbar scrollbar-primary mx-auto" style={{ maxHeight: '350px' }}>
            {/* --Lista de Solicitudes-- */}
            <MDBTable hover >
              <MDBTableBody >
                {
                  this.state.solicitud.map(sol =>
                    this.state.estadoSol == "Todas" ? //Filtro por Estado
                      this.props.Us.cargo == "Solicitante" ? //Si es solicitante muestra sólo sus propias solicitudes
                        sol.usuKey == this.props.KeyUs ?
                          <MDBCard className="p-2 m-1">
                            <MDBRow className="mx-1">
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar" size="2" /> #{sol.tipo_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon icon="exclamation-triangle" size="2" /> {sol.desc_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon className="pr-1" icon="user" size="2" />
                                  {this.state.usuario.map(us => us.key == sol.usuKey ? us.nombres + ' ' + us.apellidos : null)}</h6>{/*If para rescatar los nombres de los usuarios */}
                                <h6 className="listaSolT"><MDBIcon far icon="clock" size="2" /> {sol.estado}</h6>
                              </MDBCol>
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar-alt" size="2" /> {Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(sol.fecha_emision)}</h6>
                                <h6 className="listaSolT"><MDBIcon far icon="building" size="2" /> {sol.area_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="money-bill-alt"></MDBIcon><NumberFormat value={sol.total_sol} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="clipboard"></MDBIcon>{sol.estadoVal}</h6>
                              </MDBCol>
                              <MDBCol className="col-3 mt-3">
                                <MDBRow className="px-2">
                                <MDBCol className="mb-1">
                                  <MDBBtn block color={sol.estadoVal == "Por Validar" && this.props.Us.cargo == "Jefe Administrativo" ? "yellow" :
                                    sol.estadoVal == "Validada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                      sol.estadoVal == "Generada" && this.props.Us.cargo == "Administrador" ? "yellow" :
                                        sol.estadoVal == "Liberada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                          sol.estadoVal == "Completada" && this.props.Us.cargo == "Solicitante" ? "yellow" :
                                            "primary"} href={`/versolicitud/${sol.key}`} className="px-2" size="sm">Ver <MDBIcon far icon="arrow-alt-circle-right" /></MDBBtn>
                                  </MDBCol>
                                  <MDBCol >
                                    <MDBBtn block color={
                                      sol.estadoVal == "Por Validar" ? "primary" :
                                        sol.estadoVal == "Generada" ? "default" :
                                          sol.estadoVal == "Rechazada" ? "danger" :
                                            sol.estadoVal == "Validada" ? "secondary" :
                                              sol.estadoVal == "Liberada" ? "light-green" :
                                                sol.estadoVal == "Completada" ? "light-green" :
                                                  sol.estadoVal == "Confirmada" ? "dark-green" : "warning"} size="sm" className="px-2" disabled outline><strong>{sol.estadoVal}</strong>
                                    </MDBBtn>
                                  </MDBCol>
                                </MDBRow>
                              </MDBCol>

                            </MDBRow>
                          </MDBCard> : null
                        : //Sino Muestra todas las solicitudes
                        <MDBCard className="p-2 m-1">
                            <MDBRow className="mx-1">
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar" size="2" /> #{sol.tipo_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon icon="exclamation-triangle" size="2" /> {sol.desc_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon className="pr-1" icon="user" size="2" />
                                  {this.state.usuario.map(us => us.key == sol.usuKey ? us.nombres + ' ' + us.apellidos : null)}</h6>{/*If para rescatar los nombres de los usuarios */}
                                <h6 className="listaSolT"><MDBIcon far icon="clock" size="2" /> {sol.estado}</h6>
                              </MDBCol>
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar-alt" size="2" /> {Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(sol.fecha_emision)}</h6>
                                <h6 className="listaSolT"><MDBIcon far icon="building" size="2" /> {sol.area_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="money-bill-alt"></MDBIcon><NumberFormat value={sol.total_sol} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="clipboard"></MDBIcon>{sol.estadoVal}</h6>
                              </MDBCol>
                              <MDBCol className="col-3 mt-3">
                                <MDBRow className="px-2">
                                <MDBCol className="mb-1">
                                  <MDBBtn block color={sol.estadoVal == "Por Validar" && this.props.Us.cargo == "Jefe Administrativo" ? "yellow" :
                                    sol.estadoVal == "Validada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                      sol.estadoVal == "Generada" && this.props.Us.cargo == "Administrador" ? "yellow" :
                                        sol.estadoVal == "Liberada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                          sol.estadoVal == "Completada" && this.props.Us.cargo == "Solicitante" ? "yellow" :
                                            "primary"} href={`/versolicitud/${sol.key}`} className="px-2" size="sm">Ver <MDBIcon far icon="arrow-alt-circle-right" /></MDBBtn>
                                  </MDBCol>
                                  <MDBCol >
                                    <MDBBtn block color={
                                      sol.estadoVal == "Por Validar" ? "primary" :
                                        sol.estadoVal == "Generada" ? "default" :
                                          sol.estadoVal == "Rechazada" ? "danger" :
                                            sol.estadoVal == "Validada" ? "secondary" :
                                              sol.estadoVal == "Liberada" ? "light-green" :
                                                sol.estadoVal == "Completada" ? "light-green" :
                                                  sol.estadoVal == "Confirmada" ? "dark-green" : "warning"} size="sm" className="px-2" disabled outline><strong>{sol.estadoVal}</strong>
                                    </MDBBtn>
                                  </MDBCol>
                                </MDBRow>
                              </MDBCol>

                            </MDBRow>
                          </MDBCard>
                      : //Filtra por Estado seleccionado
                      sol.estadoVal == this.state.estadoSol ?
                        this.props.Us.cargo == "Solicitante" ?//Si es solicitante muestra solo sus solicitudes
                          sol.usuKey == this.props.KeyUs ?
                          <MDBCard className="p-2 m-1">
                            <MDBRow className="mx-1">
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar" size="2" /> #{sol.tipo_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon icon="exclamation-triangle" size="2" /> {sol.desc_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon className="pr-1" icon="user" size="2" />
                                  {this.state.usuario.map(us => us.key == sol.usuKey ? us.nombres + ' ' + us.apellidos : null)}</h6>{/*If para rescatar los nombres de los usuarios */}
                                <h6 className="listaSolT"><MDBIcon far icon="clock" size="2" /> {sol.estado}</h6>
                              </MDBCol>
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar-alt" size="2" /> {Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(sol.fecha_emision)}</h6>
                                <h6 className="listaSolT"><MDBIcon far icon="building" size="2" /> {sol.area_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="money-bill-alt"></MDBIcon><NumberFormat value={sol.total_sol} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="clipboard"></MDBIcon>{sol.estadoVal}</h6>
                              </MDBCol>
                              <MDBCol className="col-3 mt-3">
                                <MDBRow className="px-2">
                                <MDBCol className="mb-1">
                                  <MDBBtn block color={sol.estadoVal == "Por Validar" && this.props.Us.cargo == "Jefe Administrativo" ? "yellow" :
                                    sol.estadoVal == "Validada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                      sol.estadoVal == "Generada" && this.props.Us.cargo == "Administrador" ? "yellow" :
                                        sol.estadoVal == "Liberada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                          sol.estadoVal == "Completada" && this.props.Us.cargo == "Solicitante" ? "yellow" :
                                            "primary"} href={`/versolicitud/${sol.key}`} className="px-2" size="sm">Ver <MDBIcon far icon="arrow-alt-circle-right" /></MDBBtn>
                                  </MDBCol>
                                  <MDBCol >
                                    <MDBBtn block color={
                                      sol.estadoVal == "Por Validar" ? "primary" :
                                        sol.estadoVal == "Generada" ? "default" :
                                          sol.estadoVal == "Rechazada" ? "danger" :
                                            sol.estadoVal == "Validada" ? "secondary" :
                                              sol.estadoVal == "Liberada" ? "light-green" :
                                                sol.estadoVal == "Completada" ? "light-green" :
                                                  sol.estadoVal == "Confirmada" ? "dark-green" : "warning"} size="sm" className="px-2" disabled outline><strong>{sol.estadoVal}</strong>
                                    </MDBBtn>
                                  </MDBCol>
                                </MDBRow>
                              </MDBCol>

                            </MDBRow>
                          </MDBCard> : null
                          : // Sino Muestra todas las solicitudes
                          <MDBCard className="p-2 m-1">
                            <MDBRow className="mx-1">
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar" size="2" /> #{sol.tipo_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon icon="exclamation-triangle" size="2" /> {sol.desc_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon className="pr-1" icon="user" size="2" />
                                  {this.state.usuario.map(us => us.key == sol.usuKey ? us.nombres + ' ' + us.apellidos : null)}</h6>{/*If para rescatar los nombres de los usuarios */}
                                <h6 className="listaSolT"><MDBIcon far icon="clock" size="2" /> {sol.estado}</h6>
                              </MDBCol>
                              <MDBCol className="col-4 m-1 p-1">
                                <h6 className="listaSolT"><MDBIcon far icon="calendar-alt" size="2" /> {Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(sol.fecha_emision)}</h6>
                                <h6 className="listaSolT"><MDBIcon far icon="building" size="2" /> {sol.area_sol}</h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="money-bill-alt"></MDBIcon><NumberFormat value={sol.total_sol} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'$'} /></h6>
                                <h6 className="listaSolT"><MDBIcon far size="2" icon="clipboard"></MDBIcon>{sol.estadoVal}</h6>
                              </MDBCol>
                              <MDBCol className="col-3 mt-3">
                                <MDBRow className="px-2">
                                <MDBCol className="mb-1">
                                  <MDBBtn block color={sol.estadoVal == "Por Validar" && this.props.Us.cargo == "Jefe Administrativo" ? "yellow" :
                                    sol.estadoVal == "Validada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                      sol.estadoVal == "Generada" && this.props.Us.cargo == "Administrador" ? "yellow" :
                                        sol.estadoVal == "Liberada" && this.props.Us.cargo == "Operador" ? "yellow" :
                                          sol.estadoVal == "Completada" && this.props.Us.cargo == "Solicitante" ? "yellow" :
                                            "primary"} href={`/versolicitud/${sol.key}`} className="px-2" size="sm">Ver <MDBIcon far icon="arrow-alt-circle-right" /></MDBBtn>
                                  </MDBCol>
                                  <MDBCol >
                                    <MDBBtn block color={
                                      sol.estadoVal == "Por Validar" ? "primary" :
                                        sol.estadoVal == "Generada" ? "default" :
                                          sol.estadoVal == "Rechazada" ? "danger" :
                                            sol.estadoVal == "Validada" ? "secondary" :
                                              sol.estadoVal == "Liberada" ? "light-green" :
                                                sol.estadoVal == "Completada" ? "light-green" :
                                                  sol.estadoVal == "Confirmada" ? "dark-green" : "warning"} size="sm" className="px-2" disabled outline><strong>{sol.estadoVal}</strong>
                                    </MDBBtn>
                                  </MDBCol>
                                </MDBRow>
                              </MDBCol>

                            </MDBRow>
                          </MDBCard> : null
                  )
                }
              </MDBTableBody>
            </MDBTable>
          </div>
        </MDBCardBody>
      </MDBCard>
    )
  }
}

export default ListaSolicitudDashboard;