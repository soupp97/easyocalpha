import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/functions';



/*--Configuración de la base de datos--*/
const db_config = {
    apiKey: "AIzaSyBvckhpghlKuMMjIxDN5rWXaeJboWmI4_0",
  authDomain: "ezsoc-80bf5.firebaseapp.com",
  databaseURL: "https://ezsoc-80bf5.firebaseio.com",
  projectId: "ezsoc-80bf5",
  storageBucket: "ezsoc-80bf5.appspot.com",
  messagingSenderId: "866104194569",
  appId: "1:866104194569:web:d4e2b70ef8bd1e5a"
};
/*--Inicializar otra conexión, para la creación de usuarios, esto sirve para no reemplazar la conexión del admin mientras ingresa un nuevo usuario--*/
const FirebaseUsers = firebase.initializeApp(db_config, "Secundario");
/*--Firebase aún no permite buscar usuarios de la base de datos para cambiar su correo y contraseña, por lo que su propio perfil debe ser modificado por el mismo usuario--*/

 /*--Exportación de la configuración */
 export default FirebaseUsers;
