import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/functions';



/*--Instancia de nuestra base de datos--*/
const db_config = {
    apiKey: "AIzaSyBvckhpghlKuMMjIxDN5rWXaeJboWmI4_0",
  authDomain: "ezsoc-80bf5.firebaseapp.com",
  databaseURL: "https://ezsoc-80bf5.firebaseio.com",
  projectId: "ezsoc-80bf5",
  storageBucket: "ezsoc-80bf5.appspot.com",
  messagingSenderId: "866104194569",
  appId: "1:866104194569:web:d4e2b70ef8bd1e5a"
};
/*--Inicializamos la conexión--*/
const Firebase = firebase.initializeApp(db_config);

 /*--Actualización de la configuración */
 export default Firebase;
