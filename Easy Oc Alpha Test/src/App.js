import React, { Component } from 'react';
import Routes from '../src/components/master/Routes';
import TopNavigation from './components/master/topNavigation';
import Firebase from './Firebase';
import './index.css';
import { userInfo } from 'os';
import Login from './components/pages/Login';
import BreadcrumSection from './components/master/BreadcrumSection';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usuario: [],//Datos del usuario
      CECO:[],
      currentUs: {},
      usActual: '',
      key:'',
      keyA:'',//Key del usuario actual
      keyUs: '',//Key del usuario
      contenido: '',//Variable que muestra contenido según si está logeado
      cargoLogin: '',//Variable que rescata el cargo
      user: false//variable asignada para el usuario si está logeado
    }

  }
  _remove(position) {
    let { data } = this.state;

    let newData = [
      ...data.slice(0, position),
      ...data.slice(position + 1),
    ]

    this.setState({ data: newData });
  }


  obtenerUsuarios() {
    Firebase.firestore().collection('usuario').get().then((snapShots) => {
      this.setState({
        usuario: snapShots.docs.map(doc => {
          return {
            keyUs: doc.id,
            data: doc.data()
          }
        }
        )
      })
      this.authListener();
    })
  }

  obtenerCECO() {
    Firebase.firestore().collection('centro_costo').get().then((snapShots) => {
      this.setState({
        CECO: snapShots.docs.map(doc => {
          return {
            key: doc.id,
            data: doc.data()
          }
        }
        )
      })      
    })
  }

  




  componentDidMount() {
    this.obtenerUsuarios();
    this.obtenerCECO();
    
    


  }
  /*Función para Validación de Credenciales */
  authListener() {
    Firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        const ref = Firebase.firestore().collection('usuario').doc(user.uid);
        ref.get().then((doc) => {
          if (doc.exists) {
            this.setState({
              currentUs: doc.data(),
              keyA: doc.id,
              isLoading: false
            });
            
            this.setState({ user });
        
        this.setState({ contenido: <div><BreadcrumSection /><Routes data={this.state} /></div> });
        } else {
          console.log("No such document!");
        }
      });
        
        

      } else {
        this.setState({ user: null });
        this.setState({ contenido: <Login data={this.state} /> });
      }
    });
    

  }

  render() {
    return (
      <div className="flexible-content">
        <TopNavigation hiddenbtn={this.state.user} />
        <main id="content" className="p-4">
          {this.state.contenido}
        </main>
      </div>
    );
  }
}

export default App;
